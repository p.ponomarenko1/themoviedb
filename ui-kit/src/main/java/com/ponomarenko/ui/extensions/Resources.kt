package com.ponomarenko.ui.extensions

import android.animation.AnimatorInflater
import android.animation.StateListAnimator
import android.content.res.ColorStateList
import android.view.View
import androidx.annotation.AnimatorRes
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.fragment.app.Fragment
import com.redmadrobot.extensions.resources.getColor
import com.redmadrobot.extensions.resources.getColorStateList
import com.redmadrobot.extensions.resources.resolveColor
import com.redmadrobot.extensions.resources.resolveResourceId

public fun View.loadStateListAnimator(@AnimatorRes res: Int): StateListAnimator? {
    return AnimatorInflater.loadStateListAnimator(context, res)
}

@ColorInt
public fun Fragment.resolveColor(@AttrRes attr: Int): Int {
    return requireContext().resolveColor(attr)
}

@ColorInt
public fun View.resolveColor(@AttrRes attr: Int): Int {
    return context.resolveColor(attr)
}

public fun View.resolveResourceId(@AttrRes attr: Int): Int {
    return context.resolveResourceId(attr)
}

/**
 * Returns [ColorStateList] if given [res] is ColorStateList.
 * Otherwise, if the [res] is plain color, returns new [ColorStateList] with only this color.
 */
public fun View.getOrCreateColorStateList(@ColorRes res: Int): ColorStateList {
    return getColorStateList(res) ?: ColorStateList.valueOf(getColor(res))
}
