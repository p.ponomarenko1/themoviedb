package com.ponomarenko.ui.snackbar

import android.content.Context
import android.content.res.ColorStateList
import android.widget.TextView
import androidx.annotation.ColorInt
import com.google.android.material.snackbar.Snackbar
import com.ponomarenko.ui.R
import com.redmadrobot.extensions.resources.resolveColor

fun Snackbar.setStyle(style: SnackbarStyle) {

    val textView = view.findViewById<TextView>(R.id.snackbar_text)

    // Убираем лимит строк
    textView?.maxLines = Int.MAX_VALUE
    textView?.ellipsize = null

    // Изменение фона и тени
    val backgroundColor = style.getBackgroundColor(context)
    view.backgroundTintList = ColorStateList.valueOf(backgroundColor)
    view.outlineAmbientShadowColor = backgroundColor
    view.outlineSpotShadowColor = backgroundColor

    // Изменение цвета текста
    val textColor = style.getTextColor(context)
    val actionTextView = view.findViewById<TextView>(R.id.snackbar_action)

    textView?.setTextColor(textColor)
    actionTextView?.setTextColor(textColor)
}

sealed class SnackbarStyle {
    @ColorInt
    internal abstract fun getBackgroundColor(context: Context): Int

    @ColorInt
    internal abstract fun getTextColor(context: Context): Int

    object Error : SnackbarStyle() {
        override fun getBackgroundColor(context: Context): Int =
            context.resolveColor(R.attr.colorError)

        override fun getTextColor(context: Context): Int = context.resolveColor(R.attr.colorOnError)
    }

    object Message : SnackbarStyle() {
        override fun getBackgroundColor(context: Context): Int =
            context.getColor(R.color.movie_dark_blue_500)

        override fun getTextColor(context: Context): Int =
            context.resolveColor(R.attr.colorOnPrimary)
    }

    object UrgentMessage : SnackbarStyle() {
        override fun getBackgroundColor(context: Context): Int =
            context.getColor(R.color.movie_violet_400)

        override fun getTextColor(context: Context): Int = context.resolveColor(R.attr.colorOnError)

    }
}