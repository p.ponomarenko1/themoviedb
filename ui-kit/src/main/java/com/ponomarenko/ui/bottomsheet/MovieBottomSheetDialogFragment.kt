package com.ponomarenko.ui.bottomsheet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.annotation.LayoutRes
import androidx.core.view.doOnLayout
import androidx.core.view.updateLayoutParams
import androidx.core.view.updatePadding
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.ponomarenko.ui.R
import com.ponomarenko.ui.bottomsheet.MovieBottomSheetDialogFragment.BottomSheetType.*

/** BottomSheet с возможностью включения полноэкранного режима. */
abstract class MovieBottomSheetDialogFragment protected constructor(
    @LayoutRes private val layoutId: Int,
) : BottomSheetDialogFragment() {

    /**
     * Тип BottomSheet Можно изменить, переопределив его в классе-наследнике.
     * @see BottomSheetType
     */
    protected open val bottomSheetType: BottomSheetType = DEFAULT

    protected val behavior: BottomSheetBehavior<FrameLayout>
        get() = (dialog as BottomSheetDialog).behavior

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        when (bottomSheetType) {
            DEFAULT -> Unit
            FULLSCREEN -> setStyle(
                STYLE_NO_TITLE,
                R.style.ThemeOverlay_Movie_BottomSheetDialog_Fullscreen
            )

            FULLSCREEN_IGNORE_SYSTEM_WINDOWS -> {
                setStyle(
                    STYLE_NO_TITLE,
                    R.style.ThemeOverlay_Movie_BottomSheetDialog_Fullscreen_TranslucentNavigation
                )
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        require(layoutId != 0) { "Parameter 'layoutId' should not be 0" }

        return if (bottomSheetType == FULLSCREEN) {
            val view =
                inflater.inflate(R.layout.movie_dialog_bottom_sheet_fullscreen, container, false)
            inflater.inflate(layoutId, view.findViewById(R.id.fullscreen_bottom_sheet_container))
            view.findViewById<View>(R.id.fullscreen_bottom_sheet_close_button)
                .setOnClickListener { dismiss() }
            view
        } else {
            inflater.inflate(layoutId, container, false)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (bottomSheetType == FULLSCREEN || bottomSheetType == FULLSCREEN_IGNORE_SYSTEM_WINDOWS) {
            view.doOnLayout {
                switchToFullscreen()
                // Trigger parent view layout update
                ((it as ViewGroup).getChildAt(0) ?: it).requestLayout()

                if (bottomSheetType == FULLSCREEN_IGNORE_SYSTEM_WINDOWS) {
                    ignoreSystemWindows()
                }
            }
        }
    }

    private fun ignoreSystemWindows() {
        var parent = requireView().parent as? View
        while (parent != null) {
            parent.fitsSystemWindows = false
            parent.updatePadding(top = 0, bottom = 0)
            parent = parent.parent as? View
        }
    }

    private fun switchToFullscreen() {
        with(behavior) {
            state = BottomSheetBehavior.STATE_EXPANDED
            skipCollapsed = true
            isFitToContents = false
        }

        requireDialog().requireViewById<FrameLayout>(R.id.design_bottom_sheet)
            .updateLayoutParams { height = ViewGroup.LayoutParams.MATCH_PARENT }
    }


    protected enum class BottomSheetType {
        /** Обычный боттом-шит */
        DEFAULT,

        /** Полноэкранный боттом-шит, с отступом и крестиком для закрытия сверху */
        FULLSCREEN,

        /** Полноэкранный боттом-шит, рисующийся под системным UI */
        FULLSCREEN_IGNORE_SYSTEM_WINDOWS
    }
}