plugins {
    id("com.android.library")
    id("base-android-convention")
}

android {
    buildFeatures.androidResources = true
}

dependencies {
    implementation("com.google.android.material:material:1.3.0")
    implementation("androidx.core:core-ktx:1.5.0")
    implementation("androidx.activity:activity-ktx:1.2.3")
    implementation("com.redmadrobot.extensions:resources-ktx:1.2.0-1")
}