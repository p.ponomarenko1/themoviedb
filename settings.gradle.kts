pluginManagement {
    repositories {
        mavenCentral()
        gradlePluginPortal()
        google()
    }

     resolutionStrategy.eachPlugin {
        val id = requested.id.id
        when {
            id.startsWith("org.jetbrains.kotlin") ->
                useModule("org.jetbrains.kotlin:kotlin-gradle-plugin:${requested.version}")
            id.startsWith("com.android.tools.build") ->
                useModule("com.android.tools.build:gradle:${requested.version}")
            id.startsWith("io.gitlab.arturbosch.detekt") ->
                useModule("io.gitlab.arturbosch.detekt:detekt-gradle-plugin:${requested.version}")
            id.startsWith("dagger.hilt") ->
                useModule("com.google.dagger:hilt-android-gradle-plugin:${requested.version}")
            id.startsWith("androidx.navigation.safeargs") ->
                useModule("androidx.navigation:navigation-safe-args-gradle-plugin:${requested.version}")
            id.startsWith("de.mannodermaus.android-junit5") ->
                useModule("de.mannodermaus.gradle.plugins:android-junit5:${requested.version}")
            id.startsWith("com.google.gms") ->
                useModule("com.google.gms:google-services:${requested.version}")
        }
    }
}

dependencyResolutionManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}

rootProject.name = "TheMovieDB"
include(":app")
include(":ui-kit")
