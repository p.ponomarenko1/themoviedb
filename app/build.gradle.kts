import com.android.build.api.dsl.VariantDimension
import org.jetbrains.kotlin.konan.properties.Properties

plugins {
    id("android-application-convention")
    id("io.gitlab.arturbosch.detekt")
    kotlin("android")
    id("kotlin-parcelize")
    kotlin("kapt")
    id("kotlin-android")
}

val keystoreProperties = Properties()
keystoreProperties.load(file("$rootDir/keystore.properties").inputStream())


apply(plugin = "dagger.hilt.android.plugin")
apply(plugin = "androidx.navigation.safeargs.kotlin")
apply(plugin = "com.google.gms.google-services")
apply(plugin = "com.google.firebase.crashlytics")

android {
    defaultConfig {
        applicationId = "com.ponomarenko.themoviedb"
    }
    buildFeatures {
        viewBinding = true
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            setApiV3Key()
            setYoutubeApiKey()
            setBaseUrl()
            setBasePosterUrl()
            setBaseTrailerImageUrl()
        }

        getByName("debug") {
            buildConfigField("boolean", "CRASH_REPORTS_ENABLED", "true")
            setApiV3Key()
            setYoutubeApiKey()
            setBaseUrl()
            setBasePosterUrl()
            setBaseTrailerImageUrl()
        }
    }
}


fun VariantDimension.setApiV3Key() =
    buildConfigField("String", "API_V3_KEY", "${keystoreProperties["THE_MOVIE_DATABASE_API"]}")

fun VariantDimension.setYoutubeApiKey() =
    buildConfigField("String", "YOUTUBE_API_KEY", "${keystoreProperties["YOUTUBE_API_KEY"]}")

fun VariantDimension.setBaseUrl() = buildConfigField("String", "BASE_URL", "\"https://api.themoviedb.org/3/\"")
fun VariantDimension.setBasePosterUrl() = buildConfigField("String", "POSTER_BASE_URL", "\"https://image.tmdb.org/t/p/w1280/\"")
fun VariantDimension.setBaseTrailerImageUrl() = buildConfigField("String", "TRAILER_IMAGE_BASE_URL", "\"https://img.youtube.com/vi/%s/sddefault.jpg\"")

detekt {
    baseline = file("$rootDir/detekt-baseline.xml")
}

kapt {
    correctErrorTypes = true
}

dependencies {

    implementation(platform(kotlin("bom")))

    // Base
    implementation(androidx.activity)
    implementation(androidx.annotation)
    implementation(androidx.appcompat)
    implementation(androidx.core)
    implementation(androidx.fragment)
    implementation(androidx.lifecycle.process)
    implementation(redmadrobot.core_ktx)
    implementation(redmadrobot.livedata_ktx)
    implementation(redmadrobot.resources_ktx)
    implementation(redmadrobot.viewbinding_ktx)
    implementation(redmadrobot.fragment_args_ktx)

    // UI
    implementation(androidx.material)
    implementation(androidx.recyclerview)
    implementation(androidx.swiperefreshlayout)
    implementation(androidx.constraintlayout)
    implementation(androidx.coordinatorlayout)
    implementation(misc.insetter)
    implementation(coil)
    implementation(redmadrobot.itemsadapter)
    implementation(airbnb.lottie)
    implementation(airbnb.epoxy)
    implementation(airbnb.epoxy.paging3)
    implementation("org.jetbrains.kotlin:kotlin-stdlib:${rootProject.extra["kotlin_version"]}")
    implementation("androidx.legacy:legacy-support-v4:1.0.0")
    implementation(project(mapOf("path" to ":ui-kit")))
    implementation(files("libs/YouTubeAndroidPlayerApi.jar"))
    kapt(airbnb.epoxy.processor)

    // Navigation
    implementation(androidx.navigation.fragment_ktx)
    implementation(androidx.navigation.ui_ktx)

    // Network
    implementation(square.okhttp)
    implementation(square.retrofit2)
    implementation(square.retrofit2.converter_moshi)
    implementation(square.moshi)
    kapt(square.moshi.kotlin_codegen)

    // Tools
    implementation(kotlinx.coroutines_android)
    implementation(androidx.paging3)
    implementation(androidx.room.ktx)
    kapt(androidx.room.compiler)
    implementation(redmadrobot.pinkman.coroutines)
    implementation(redmadrobot.flipper)
    implementation(redmadrobot.mapmemory.coroutines)
    implementation(redmadrobot.mapmemory.kapt_bug_workaround)

    // DI
    implementation(google.dagger.hilt_android)
    implementation(androidx.hilt.navigation_fragment)
    kapt(google.dagger.hilt_compiler)

    // Debug
    implementation(square.okhttp.logging_interceptor)
    implementation(misc.logger)
    implementation(misc.timber)
    debugImplementation(square.leakcanary)

    // Firebase and analytics
    implementation(platform(google.firebase.bom))
    implementation(google.firebase.crashlytics)
    implementation(google.firebase.analytics)
    implementation(google.firebase.messaging)

    // Testing
    testImplementation(junit.jupiter.api)
    testImplementation(junit.jupiter.params)
    testImplementation(mockito.core)
    testImplementation(mockito.kotlin)
    testImplementation(mockito.inline)
    testRuntimeOnly(junit.jupiter.engine)
}

configurations.all {
    resolutionStrategy.eachDependency {
        // Не повышать версию до фикса https://issuetracker.google.com/issues/180950116
        if (requested.group == "androidx.paging") useVersion("3.0.0-alpha12")
    }
}