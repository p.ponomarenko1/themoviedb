
package com.ponomarenko.themoviedb.app.di

import com.ponomarenko.themoviedb.BuildConfig
import com.ponomarenko.themoviedb.base.data.api.AuthHeaderInterceptor
import com.ponomarenko.themoviedb.base.data.api.EnvelopeJsonConverter
import com.ponomarenko.themoviedb.base.data.api.SerializeNullsFactory
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    private const val DEFAULT_TIMEOUT_SECONDS = 60L

    @Provides
    @Singleton
    fun provideRetrofit(
        moshi: Moshi,
        authHeaderInterceptor: AuthHeaderInterceptor,
    ): Retrofit {
        val httpClient = createOkHttpClientBuilder()
            .addInterceptor(authHeaderInterceptor)
            .build()

        return createRetrofit(httpClient, moshi)
    }

    @Provides
    fun provideMoshi(): Moshi {
        return Moshi.Builder()
            .add(SerializeNullsFactory())
            .build()
    }

    private fun createOkHttpClientBuilder(): OkHttpClient.Builder {
        val loggingInterceptor = HttpLoggingInterceptor().apply {
            if (BuildConfig.DEBUG) setLevel(HttpLoggingInterceptor.Level.BODY)
        }
        return OkHttpClient.Builder()
            .connectTimeout(DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .readTimeout(DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .writeTimeout(DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .addInterceptor(loggingInterceptor)
    }

    private fun createRetrofit(httpClient: OkHttpClient, moshi: Moshi): Retrofit {
        return Retrofit.Builder()
            .client(httpClient)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addConverterFactory(EnvelopeJsonConverter.Factory())
            .baseUrl(BuildConfig.BASE_URL)
            .build()
    }

}
