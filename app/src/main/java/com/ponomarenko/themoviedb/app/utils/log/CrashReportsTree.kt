package com.ponomarenko.themoviedb.app.utils.log

import android.util.Log
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import timber.log.Timber

class CrashReportsTree : Timber.Tree() {

    override fun isLoggable(tag: String?, priority: Int): Boolean = priority >= Log.INFO

    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
       Firebase.crashlytics.log(message)
        if (t != null && priority >= Log.WARN) Firebase.crashlytics.recordException(t)
    }
}
