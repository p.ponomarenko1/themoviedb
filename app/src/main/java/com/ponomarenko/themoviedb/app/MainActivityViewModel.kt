package com.ponomarenko.themoviedb.app

import com.ponomarenko.themoviedb.app.di.AppSharedEvents
import com.ponomarenko.themoviedb.base.presentation.viewmodel.StatefulViewModel
import com.redmadrobot.extensions.lifecycle.EventQueue
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor(
    @AppSharedEvents val appEvents: EventQueue,
) : StatefulViewModel<Nothing>()
