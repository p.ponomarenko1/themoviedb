package com.ponomarenko.themoviedb.app.utils.extentions

import java.util.*

fun <T> Optional<T>.getOrNull(): T? {
    return runIf(isPresent) { get()!! }
}
