package com.ponomarenko.themoviedb.app.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ponomarenko.themoviedb.feature.base.data.MoviesDao
import com.ponomarenko.themoviedb.feature.base.data.entity.MovieDbEntity

@Database(version = 1, entities = [MovieDbEntity::class])
abstract class AppDatabase : RoomDatabase() {
    abstract val moviesDao: MoviesDao
}
