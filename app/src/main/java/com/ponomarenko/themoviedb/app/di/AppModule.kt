
package com.ponomarenko.themoviedb.app.di

import android.content.Context
import androidx.room.Room
import com.ponomarenko.themoviedb.app.data.AppDatabase
import com.ponomarenko.themoviedb.base.data.AndroidResourceReader
import com.ponomarenko.themoviedb.base.domain.ResourceReader
import com.redmadrobot.extensions.lifecycle.EventQueue
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    fun provideResourceReader(@ApplicationContext context: Context): ResourceReader =
        AndroidResourceReader(context)

    @Provides
    @Singleton
    @AppSharedEvents
    fun provideAppSharedEvents() = EventQueue()

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, "database").build()
    }
}

@Qualifier
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class AppSharedEvents
