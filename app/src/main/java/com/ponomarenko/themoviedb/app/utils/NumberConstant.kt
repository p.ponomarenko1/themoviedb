package com.ponomarenko.themoviedb.app.utils

const val MIN_SEARCH_QUERY_LENGTH = 2
const val SEARCH_INPUT_DELAY_MILLIS = 500L

