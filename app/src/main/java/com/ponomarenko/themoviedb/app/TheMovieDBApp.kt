package com.ponomarenko.themoviedb.app

import android.app.Application
import coil.ImageLoader
import coil.ImageLoaderFactory
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.google.firebase.ktx.initialize
import com.ponomarenko.themoviedb.BuildConfig
import com.ponomarenko.themoviedb.app.utils.log.CrashReportsTree
import com.ponomarenko.themoviedb.app.utils.log.PrettyLoggingTree
import com.ponomarenko.themoviedb.base.data.coil.EncodedImageFetcher
import com.ponomarenko.themoviedb.feature.toggles.LocalFlipperConfig
import com.redmadrobot.flipper.ToggleRouter
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class TheMovieDBApp : Application(), ImageLoaderFactory {

    private val isMainProcess: Boolean
        get() = getProcessName() == packageName

    override fun onCreate() {
        super.onCreate()

        if (isMainProcess) {
            initFirebase()
            initFlipper()
            initTimber()
        }
    }

    override fun newImageLoader(): ImageLoader {
        return ImageLoader.Builder(this)
             .componentRegistry {
                 add(EncodedImageFetcher())
             }
            .build()
    }

    private fun initFirebase() {
        Firebase.initialize(this)?.apply {
            setAutomaticResourceManagementEnabled(true)
        }
        Firebase.crashlytics.setCrashlyticsCollectionEnabled(BuildConfig.CRASH_REPORTS_ENABLED)
    }

    private fun initFlipper() {
        ToggleRouter.init(LocalFlipperConfig())
    }

    private fun initTimber() {
        when {
            BuildConfig.DEBUG -> Timber.plant(PrettyLoggingTree())
            BuildConfig.CRASH_REPORTS_ENABLED -> Timber.plant(CrashReportsTree())
        }
    }
}
