package com.ponomarenko.themoviedb.app.utils.extentions

inline fun <T> runIf(condition: Boolean, ifTrue: () -> T): T? {
    return if (condition) ifTrue() else null
}
