package com.ponomarenko.themoviedb.app.utils.type

/**
 * Может содержать в себе либо объект типа L, либо объект типа R. Удобен, чтобы не плодить
 * sealed классы в тривиальных случаях.
 * Пример:
 * ```
 * val numberOrString = if (...) 1.left() else "hello".right()
 * numberOrString.fold(
 *  { number[: Int] -> doSomethingWithNumber(number) },
 *  { string[: String] -> doSomethingWithString(string) }
 * )
 * ```
 */

sealed class Either<out L, out R> {

    data class Left<out L>(val value: L) : Either<L, Nothing>()
    data class Right<out R>(val value: R) : Either<Nothing, R>()

    fun <C> fold(ifLeft: (L) -> C, ifRight: (R) -> C): C {
        return when (this) {
            is Left -> ifLeft(value)
            is Right -> ifRight(value)
        }
    }

    fun <C> ifLeft(ifLeft: (L) -> C): C? {
        return when (this) {
            is Left -> ifLeft(value)
            is Right -> null
        }
    }

    fun <C> ifRight(ifRight: (R) -> C): C? {
        return when (this) {
            is Left -> null
            is Right -> ifRight(value)
        }
    }
}

fun <T> T.left() = Either.Left(this)
fun <T> T.right() = Either.Right(this)
