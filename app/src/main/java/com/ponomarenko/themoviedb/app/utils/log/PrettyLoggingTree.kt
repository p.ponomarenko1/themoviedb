package com.ponomarenko.themoviedb.app.utils.log

import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.orhanobut.logger.PrettyFormatStrategy
import timber.log.Timber

class PrettyLoggingTree : Timber.Tree(){

    companion object {
        private const val TIMBER_CALL_CAIN_LENGTH = 5
    }

    init {
        val formatStrategy = PrettyFormatStrategy.newBuilder()
            .tag("MOVIES")
            .methodOffset(TIMBER_CALL_CAIN_LENGTH)
            .build()

        Logger.addLogAdapter(AndroidLogAdapter(formatStrategy))
    }

    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        Logger.log(priority, tag, message, t)
    }
}
