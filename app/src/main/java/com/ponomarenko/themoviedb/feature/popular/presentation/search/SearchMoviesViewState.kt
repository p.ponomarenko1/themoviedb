package com.ponomarenko.themoviedb.feature.popular.presentation.search

import androidx.paging.PagingData
import com.ponomarenko.themoviedb.feature.base.presentation.MoviesViewState
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Movie

data class SearchMoviesViewState(
    override val item: PagingData<Movie>
) : MoviesViewState<Movie>
