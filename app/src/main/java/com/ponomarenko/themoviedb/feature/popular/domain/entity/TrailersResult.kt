package com.ponomarenko.themoviedb.feature.popular.domain.entity

data class TrailersResult(
    val id: Long,
    val results: List<Trailer>
)
