package com.ponomarenko.themoviedb.feature.popular.domain.entity

data class ProductionCompany(
    val id: Int,
    val logoPath: String?,
    val name: String,
    val originCountry: String
)
