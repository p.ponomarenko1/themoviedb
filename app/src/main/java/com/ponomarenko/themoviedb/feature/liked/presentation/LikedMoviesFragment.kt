package com.ponomarenko.themoviedb.feature.liked.presentation

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.PagingData
import com.ponomarenko.themoviedb.R
import com.ponomarenko.themoviedb.base.presentation.extensions.setupLinearLayoutManager
import com.ponomarenko.themoviedb.base.presentation.ui.BaseFragment
import com.ponomarenko.themoviedb.databinding.FragmentLikedMoviesBinding
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Movie
import com.ponomarenko.themoviedb.feature.popular.presentation.PagingMoviesEpoxyController
import com.redmadrobot.extensions.lifecycle.observe
import com.redmadrobot.extensions.viewbinding.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ObsoleteCoroutinesApi

@ObsoleteCoroutinesApi
@AndroidEntryPoint
class LikedMoviesFragment : BaseFragment(R.layout.fragment_liked_movies) {

    private val viewModel: LikedMoviesViewModel by viewModels()
    private val binding: FragmentLikedMoviesBinding by viewBinding()

    private val epoxyController by lazy {
        PagingMoviesEpoxyController(
            onMovieClicked = viewModel::onMovieClicked
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycler()

        observe(viewModel.movies, ::renderItems)
        observe(viewModel.events, ::onEvent)
    }

    override fun onResume() {
        super.onResume()
        viewModel.onResume()
    }

    private fun initRecycler() {
        with(binding.moviesEpoxyList) {
            setupLinearLayoutManager()
            setController(epoxyController)
        }
    }

    private fun renderItems(items: PagingData<Movie>?) {
        if (items != null) {
            lifecycleScope.launchWhenStarted { epoxyController.submitData(items) }
        }
    }
}
