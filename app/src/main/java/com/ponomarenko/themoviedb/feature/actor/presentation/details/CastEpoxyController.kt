package com.ponomarenko.themoviedb.feature.actor.presentation.details

import com.airbnb.epoxy.Typed2EpoxyController
import com.ponomarenko.themoviedb.R
import com.ponomarenko.themoviedb.base.domain.lce.Lce
import com.ponomarenko.themoviedb.base.presentation.ui.view.imageTextCell
import com.ponomarenko.themoviedb.base.presentation.ui.view.layoutShimmers
import com.ponomarenko.themoviedb.base.presentation.ui.view.stubView
import com.ponomarenko.themoviedb.feature.actor.domain.entity.CastPerson

class CastEpoxyController(
    private val onMovieClicked: (movieId: Long) -> Unit
) : Typed2EpoxyController<Lce<Unit>, List<CastPerson>>() {

    override fun buildModels(state: Lce<Unit>?, items: List<CastPerson>) {
        when (state) {
            Lce.Loading -> layoutShimmers(R.layout.shimmer_image_one_text_cell, true, SHIMMERS_COUNT)
            is Lce.Content -> {
                for (item in items) {
                    imageTextCell {
                        id(item.id)
                        title(item.originalTitle)
                        image(item.posterPath)
                        onClickListener { onMovieClicked(item.id) }
                    }
                }
                if (items.isEmpty()) stubView
            }
            is Lce.Error -> stubView
        }
    }

    val stubView
    get() = stubView {
        id("stub_error")
        image(R.drawable.image_error_loading)
        title(R.string.movies_error)
        text(R.string.data_loading_error)
    }
}

private const val SHIMMERS_COUNT = 6
