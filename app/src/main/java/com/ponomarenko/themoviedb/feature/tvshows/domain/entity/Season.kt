package com.ponomarenko.themoviedb.feature.tvshows.domain.entity

data class Season(
    val airDate: String?,
    val episodeCount: Int,
    val id: Int,
    val name: String,
    val overview: String,
    val posterPath: String?,
    val seasonNumber: Int
)
