package com.ponomarenko.themoviedb.feature.popular.presentation.details

import com.airbnb.epoxy.Typed2EpoxyController
import com.ponomarenko.themoviedb.R
import com.ponomarenko.themoviedb.base.domain.lce.Lce
import com.ponomarenko.themoviedb.base.presentation.ui.view.circleImageTextCell
import com.ponomarenko.themoviedb.base.presentation.ui.view.layoutShimmers
import com.ponomarenko.themoviedb.base.presentation.ui.view.stubView
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Cast

class ActorsEpoxyController(
    private val onActorClicked: (Int) -> Unit,
) : Typed2EpoxyController<Lce<Unit>, List<Cast>>() {

    override fun buildModels(state: Lce<Unit>, items: List<Cast>) {
        when (state) {
            is Lce.Loading -> {
                layoutShimmers(R.layout.shimmer_icon_text_cell, true, AVATAR_SHIMMERS_COUNT)
            }

            is Lce.Content -> {
                if (items.isEmpty()) {
                    stubView
                    return
                }
                for (entity in items) {
                    circleImageTextCell {
                        id(entity.id)
                        text(entity.originalName)
                        image(entity.profilePath)
                        onActionClickListener { onActorClicked.invoke(entity.id) }
                    }
                }
            }
            is Lce.Error -> stubView
        }
    }

    private val stubView
        get() = stubView {
            id("stub_error")
            image(R.drawable.image_error_loading)
            title(R.string.actors_error)
            text(R.string.data_loading_error)
        }
}

private const val AVATAR_SHIMMERS_COUNT = 6
