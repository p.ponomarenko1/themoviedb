package com.ponomarenko.themoviedb.feature.popular.domain.entity

data class Cast(
    val adult: Boolean,
    val gender: Int?,
    val id: Int,
    val department: String,
    val name: String,
    val originalName: String,
    val popularity: Float,
    val profilePath: String?,
    val castId: Int,
    val character: String,
    val creditId: String,
    val order: Int,
)
