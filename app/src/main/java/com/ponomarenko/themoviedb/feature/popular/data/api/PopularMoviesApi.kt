package com.ponomarenko.themoviedb.feature.popular.data.api

import com.ponomarenko.themoviedb.feature.popular.data.entity.MovieDetailsResponse
import com.ponomarenko.themoviedb.feature.popular.data.entity.PopularMovieResultResponse
import com.ponomarenko.themoviedb.feature.popular.data.entity.TrailersResultResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PopularMoviesApi {

    @GET("movie/popular")
    suspend fun getPopularMovies(@Query("page") page: Int = 1): PopularMovieResultResponse

    @GET("movie/{movie_id}")
    suspend fun getMovieDetails(@Path("movie_id") movieId: Long): MovieDetailsResponse

    @GET("movie/{movie_id}/videos")
    suspend fun getTrailersList(@Path("movie_id") movieId: Long): TrailersResultResponse

    @GET("search/movie")
    suspend fun searchMovie(
        @Query("page") page: Int = 1,
        @Query("query") query: String
    ): PopularMovieResultResponse
}
