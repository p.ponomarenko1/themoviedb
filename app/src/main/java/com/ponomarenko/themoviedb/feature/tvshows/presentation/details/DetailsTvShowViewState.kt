package com.ponomarenko.themoviedb.feature.tvshows.presentation.details

import com.ponomarenko.themoviedb.base.presentation.viewmodel.ViewState

data class DetailsTvShowViewState(
    val name: String,
    val genre: String,
    val tagline: String,
    val overview: String,
    val created: String,
    val homepage: String,
    val rating: Float,
    val imageDetail: String?,
) : ViewState
