package com.ponomarenko.themoviedb.feature.base.data

import com.ponomarenko.themoviedb.base.domain.lce.Lce
import kotlinx.coroutines.flow.Flow

interface DetailsRepository<T : Any, K : Any> {
    fun getDetails(movieId: Long): Flow<Lce<T>>
    fun saveToFaw(item: K): Flow<Lce<Unit>>
    fun delete(id: Long): Flow<Lce<Unit>>
}
