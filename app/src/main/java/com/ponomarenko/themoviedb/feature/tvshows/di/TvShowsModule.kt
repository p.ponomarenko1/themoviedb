package com.ponomarenko.themoviedb.feature.tvshows.di

import com.ponomarenko.themoviedb.feature.tvshows.data.api.TvShowsApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import retrofit2.Retrofit
import retrofit2.create

@Module
@InstallIn(ActivityRetainedComponent::class)
abstract class TvShowsModule {

    companion object {
        @Provides
        @ActivityRetainedScoped
        fun provideTopTvShowsApi(retrofit: Retrofit): TvShowsApi = retrofit.create()
    }
}
