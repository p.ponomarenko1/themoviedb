package com.ponomarenko.themoviedb.feature.base.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Movie

@Entity(tableName = "movies")
data class MovieDbEntity(
    @PrimaryKey
    val id: Long,
    val posterPath: String?,
    val adult: Boolean,
    val overview: String?,
    val originalTitle: String,
    val originalLanguage: String,
    val title: String,
    val backdropPath: String?,
    val popularity: Double,
    val releaseDate: String?,
    val voteAverage: Double
) {
    fun toDomain(): Movie =
        Movie(
            posterPath = posterPath,
            adult = adult,
            overview = overview,
            genreIds = emptyList(),
            id = id,
            originalTitle = originalTitle,
            originalLanguage = originalLanguage,
            backdropPath = backdropPath,
            popularity = popularity,
            releaseDate = releaseDate,
            voteAverage = voteAverage,
            title = title,
        )
}

