package com.ponomarenko.themoviedb.feature.tvshows.presentation.details

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import coil.load
import com.ponomarenko.themoviedb.R
import com.ponomarenko.themoviedb.app.utils.extentions.assistedViewModel
import com.ponomarenko.themoviedb.base.presentation.extensions.loadImage
import com.ponomarenko.themoviedb.base.presentation.ui.BaseFragment
import com.ponomarenko.themoviedb.databinding.FragmentTvShowDetailsBinding
import com.redmadrobot.extensions.lifecycle.observe
import com.redmadrobot.extensions.viewbinding.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DetailsTvShowFragment : BaseFragment(R.layout.fragment_tv_show_details) {

    @Inject
    lateinit var viewModelFactory: DetailsTvShowViewModel.Factory

    private val args: DetailsTvShowFragmentArgs by navArgs()
    private val binding: FragmentTvShowDetailsBinding by viewBinding()
    private val viewModel: DetailsTvShowViewModel by assistedViewModel {
        viewModelFactory.create(args.id)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observe(viewModel.name, binding.titleTv::setText)
        observe(viewModel.genre, binding.genreTv::setText)
        observe(viewModel.tagline, binding.taglineTv::setText)
        observe(viewModel.overview, binding.overviewTv::setText)
        observe(viewModel.created, binding.createdByTv::setText)
        observe(viewModel.homepage, binding.homepageTv::setText)
        observe(viewModel.rating, binding.detailRating::setRating)
        observe(viewModel.imageDetail, binding.imageDetailPreview::loadImage)
    }
}
