package com.ponomarenko.themoviedb.feature.popular.presentation.search

import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.ponomarenko.themoviedb.app.utils.MIN_SEARCH_QUERY_LENGTH
import com.ponomarenko.themoviedb.app.utils.SEARCH_INPUT_DELAY_MILLIS
import com.ponomarenko.themoviedb.base.domain.extensions.EMPTY
import com.ponomarenko.themoviedb.base.presentation.viewmodel.StatefulViewModel
import com.ponomarenko.themoviedb.feature.popular.data.MovieSearchRepository
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Movie
import com.redmadrobot.extensions.lifecycle.mapDistinct
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class SearchMovieViewModel @Inject constructor(
    private val searchRepository: MovieSearchRepository
) : StatefulViewModel<SearchMoviesViewState>() {

    private val queryFlow = MutableStateFlow(String.EMPTY)
    val items = liveState.mapDistinct { it.item }
    private val resultsStream: Flow<PagingData<Movie>> = queryFlow
        .filter { it.isNotEmpty() && it.length > MIN_SEARCH_QUERY_LENGTH }
        .debounce(SEARCH_INPUT_DELAY_MILLIS)
        .flatMapLatest { query ->
            searchRepository.searchStream(query)
        }

    init {
        state = SearchMoviesViewState(
            item = PagingData.empty()
        )
        observeSearchResultsAndState()
    }

    fun onQueryEdited(query: CharSequence?) {
        queryFlow.value = query.toString()
    }

    fun onMovieClicked(movieId: Long) {
        navigateTo(SearchMovieFragmentDirections.toDetailsMovieFragment(movieId),
            rootGraph = true)
    }

    private fun observeSearchResultsAndState() {
        resultsStream
            .cachedIn(viewModelScope)
            .onEach {
                state = state.copy(item = it)
            }
            .launchIn(viewModelScope)
    }
}
