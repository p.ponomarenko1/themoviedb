package com.ponomarenko.themoviedb.feature.popular.domain.entity

data class PopularMovieResult(
    val page: Int,
    val movies: List<Movie>
)
