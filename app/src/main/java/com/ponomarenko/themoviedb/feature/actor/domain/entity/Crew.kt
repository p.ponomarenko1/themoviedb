package com.ponomarenko.themoviedb.feature.actor.domain.entity

data class Crew(
    val id: Int,
    val department: String,
    val originalLanguage: String,
    val originalTitle: String,
    val job: String,
    val overview: String,
    val voteCount: Int,
    val video: Boolean,
    val posterPath: String?,
    val backdropPath: String?,
    val title: String,
    val popularity: Float,
    val genreIds: List<Int>,
    val voteAverage: Float,
    val adult: Boolean,
    val releaseDate: String,
    val creditId: String,
)
