package com.ponomarenko.themoviedb.feature.tvshows.domain.entity

data class TvShowOwner(
    val id: Int,
    val creditId: String,
    val name: String,
    val gender: Int,
    val profilePath: String?
)
