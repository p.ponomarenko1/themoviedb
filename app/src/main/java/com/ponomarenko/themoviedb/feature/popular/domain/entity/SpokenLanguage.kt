package com.ponomarenko.themoviedb.feature.popular.domain.entity

data class SpokenLanguage(
    val iso: String,
    val name: String
)
