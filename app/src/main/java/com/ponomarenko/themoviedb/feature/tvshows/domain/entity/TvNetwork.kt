package com.ponomarenko.themoviedb.feature.tvshows.domain.entity

data class TvNetwork(
    val name: String,
    val id: Int,
    val logoPath: String?,
    val originCountry: String
)
