package com.ponomarenko.themoviedb.feature.actor.presentation.details

import androidx.lifecycle.viewModelScope
import com.ponomarenko.themoviedb.R
import com.ponomarenko.themoviedb.base.domain.extensions.EMPTY
import com.ponomarenko.themoviedb.base.domain.lce.Lce
import com.ponomarenko.themoviedb.base.presentation.viewmodel.StatefulViewModel
import com.ponomarenko.themoviedb.feature.actor.domain.entity.ActorDetails
import com.ponomarenko.themoviedb.feature.actor.domain.entity.ActorCredits
import com.ponomarenko.themoviedb.feature.base.data.MovieActorRepository
import com.redmadrobot.extensions.lifecycle.mapDistinct
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import timber.log.Timber

class ActorDetailsViewModel @AssistedInject constructor(
    @Assisted("actorId") private val actorId: Int,
    private val actorRepository: MovieActorRepository
) : StatefulViewModel<ActorDetailsViewState>() {

    val birthday = liveState.mapDistinct { it.birthday }
    val name = liveState.mapDistinct { it.name }
    val knownAs = liveState.mapDistinct { it.knownAs }
    val biography = liveState.mapDistinct { it.biography }
    val popularity = liveState.mapDistinct { it.popularity }
    val placeOfBirth = liveState.mapDistinct { it.placeOfBirth }
    val profilePath = liveState.mapDistinct { it.profilePath }
    val screenStateWithItems = liveState.mapDistinct { it.screenState to it.cast }

    init {
        state = ActorDetailsViewState(
            birthday = String.EMPTY,
            name = String.EMPTY,
            biography = String.EMPTY,
            popularity = 0.0f,
            placeOfBirth = String.EMPTY,
            profilePath = String.EMPTY,
            knownAs = String.EMPTY,
        )
        fetchActorDetails()
        fetchPersonMovieCredits()
    }

    private fun fetchActorDetails() {
        actorRepository.getDetailsStream(actorId)
            .onEach { handleLce(it) }.launchIn(viewModelScope)
    }

    private fun fetchPersonMovieCredits() {
        actorRepository.getPersonCreditsStream(actorId)
            .onEach { newState ->
                state = if (newState is Lce.Content)
                    state.copy(
                        cast = newState.value.cast,
                        screenState = Lce.Content(Unit)
                    )
                else state.copy(screenState = reduceScreenState(state.screenState, newState))

            }.launchIn(viewModelScope)
    }

    private fun reduceScreenState(oldState: Lce<Unit>, newState: Lce<ActorCredits>): Lce<Unit> {
        return when {
            oldState is Lce.Content -> oldState
            newState is Lce.Loading -> Lce.Loading
            else -> Lce.Error(RuntimeException("Error MovieCredits downloading"))
        }
    }

    private fun handleLce(lce: Lce<ActorDetails>) {
        if (lce is Lce.Content) {
            state = state.copy(
                birthday = lce.value.birthday,
                name = lce.value.name,
                biography = lce.value.biography,
                popularity = lce.value.popularity,
                placeOfBirth = lce.value.placeOfBirth,
                profilePath = lce.value.profilePath,
                knownAs = lce.value.knownAs.joinToString { it },
            )
        } else if (lce is Lce.Error) {
            showError(R.string.details_downloading_error, false)
        }
    }

    fun onMovieClicked(movieId: Long) {
        Timber.d("onMovieClicked movieId = $movieId")
        navigateTo(
            ActorDetailsFragmentDirections.toDetailsMovieFragment(movieId = movieId),
            rootGraph = true
        )
    }

    @AssistedFactory
    interface Factory {
        fun create(
            @Assisted("actorId") actorId: Int
        ): ActorDetailsViewModel
    }
}
