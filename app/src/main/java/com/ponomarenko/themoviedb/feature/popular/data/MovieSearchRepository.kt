package com.ponomarenko.themoviedb.feature.popular.data

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.ponomarenko.themoviedb.feature.base.data.SearchRepository
import com.ponomarenko.themoviedb.feature.popular.data.api.PopularMoviesApi
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Movie
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class MovieSearchRepository @Inject constructor(
    private val api: PopularMoviesApi
): SearchRepository<Movie> {

    override fun searchStream(query: String, page: Int): Flow<PagingData<Movie>> =
        Pager(
            config = PagingConfig(
                pageSize = page,
                initialLoadSize = page + 1,
                enablePlaceholders = false,
            ),
            pagingSourceFactory = { SearchMoviesSource(query = query, api = api) }
        ).flow
}
