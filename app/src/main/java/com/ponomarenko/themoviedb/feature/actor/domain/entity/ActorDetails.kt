package com.ponomarenko.themoviedb.feature.actor.domain.entity

data class ActorDetails(
    val birthday: String?,
    val department: String,
    val deathday: String?,
    val id: Int,
    val name: String,
    val knownAs: List<String>,
    val biography: String,
    val popularity: Float,
    val placeOfBirth: String?,
    val profilePath: String?,
)
