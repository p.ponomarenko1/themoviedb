package com.ponomarenko.themoviedb.feature.popular.data

import androidx.paging.PagingSource
import com.ponomarenko.themoviedb.feature.popular.data.api.PopularMoviesApi
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Movie
import retrofit2.HttpException
import java.io.IOException
import java.net.HttpURLConnection

class PopularMoviesSource(
    private val api: PopularMoviesApi,
) : PagingSource<Int, Movie>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Movie> {
        return try {

            val nextPage = params.key ?: 1

            val response = api.getPopularMovies(nextPage).toDomain()

            LoadResult.Page(
                data = response.movies,
                prevKey = null,
                nextKey = response.page + 1
            )
        } catch (e: IOException) {
            LoadResult.Error(e)
        } catch (e: HttpException) {
            if (e.code() == HttpURLConnection.HTTP_NOT_FOUND) {
                LoadResult.Page(data = emptyList(), prevKey = null, nextKey = null)
            } else {
                LoadResult.Error(e)
            }
        }
    }
}
