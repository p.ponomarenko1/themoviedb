package com.ponomarenko.themoviedb.feature.popular.domain.entity

data class Credits(
    val id: Int,
    val cast: List<Cast>,
)
