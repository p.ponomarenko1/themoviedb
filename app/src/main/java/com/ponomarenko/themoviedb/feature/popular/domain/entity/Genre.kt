package com.ponomarenko.themoviedb.feature.popular.domain.entity

data class Genre(
    val id: Int,
    val name: String
)
