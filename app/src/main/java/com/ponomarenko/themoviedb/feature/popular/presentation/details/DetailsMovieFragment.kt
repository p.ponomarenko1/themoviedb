package com.ponomarenko.themoviedb.feature.popular.presentation.details

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import com.ponomarenko.themoviedb.R
import com.ponomarenko.themoviedb.app.utils.extentions.assistedViewModel
import com.ponomarenko.themoviedb.base.presentation.extensions.loadImage
import com.ponomarenko.themoviedb.base.presentation.extensions.setupLinearLayoutManager
import com.ponomarenko.themoviedb.base.presentation.ui.BaseFragment
import com.ponomarenko.themoviedb.databinding.FragmentMovieDetailsBinding
import com.redmadrobot.extensions.lifecycle.observe
import com.redmadrobot.extensions.viewbinding.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DetailsMovieFragment : BaseFragment(R.layout.fragment_movie_details) {

    @Inject
    lateinit var viewModelFactory: DetailsMovieViewModel.Factory

    private val args: DetailsMovieFragmentArgs by navArgs()
    private val binding: FragmentMovieDetailsBinding by viewBinding()
    private val viewModel: DetailsMovieViewModel by assistedViewModel {
        viewModelFactory.create(args.movieId)
    }

    private val actorsController by lazy {
        ActorsEpoxyController(
            onActorClicked = viewModel::onActorClicked
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUi()

        observe(viewModel.posterPath, binding.imageDetailPreview::loadImage)
        observe(viewModel.title, binding.titleTv::setText)
        observe(viewModel.overview, binding.descriptionTv::setText)
        observe(viewModel.productionCompanies, binding.studioNameTv::setText)
        observe(viewModel.genres, binding.genreNameTv::setText)
        observe(viewModel.releaseDate, binding.yearNameTv::setText)
        observe(viewModel.voteAverage, binding.detailRating::setRating)
        observe(viewModel.isLiked, binding.likeCheckBox::setChecked)
        observe(viewModel.screenStateWithItems) { (screenState, items) ->
            actorsController.setData(screenState, items)
        }
        observe(viewModel.events, ::onEvent)
    }

    private fun initUi() {
        with(binding.actorsRecyclerView) {
            setController(actorsController)
            setupLinearLayoutManager(RecyclerView.HORIZONTAL)
        }
        binding.watchBtn.setOnClickListener { viewModel.onTrailersClicked() }
        binding.likeCheckBox.setOnClickListener { viewModel.onLikeClicked(binding.likeCheckBox.isChecked) }
    }
}
