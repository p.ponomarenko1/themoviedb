package com.ponomarenko.themoviedb.feature.base.presentation

import androidx.paging.PagingData
import com.ponomarenko.themoviedb.base.domain.lce.Lce
import com.ponomarenko.themoviedb.base.presentation.viewmodel.ViewState

interface MoviesViewState<T : Any> : ViewState {
    val item: PagingData<T>
}
