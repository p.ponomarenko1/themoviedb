package com.ponomarenko.themoviedb.feature.toggles

import com.redmadrobot.flipper.Feature

object Features {

    object Popular {
        val details = SimpleFeature("movie.details")
        val trailers = SimpleFeature("movie.trailers")
        val actorDetails = SimpleFeature("movie.actor")
    }

    object TvShow {
        val tvShowDetails = SimpleFeature("tvShow.details")
    }
}

class SimpleFeature(override val id: String) : Feature()
