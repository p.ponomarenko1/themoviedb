package com.ponomarenko.themoviedb.feature.toggles

import com.redmadrobot.flipper.Feature
import com.redmadrobot.flipper.config.FlipperConfig
import com.redmadrobot.flipper.config.FlipperValue
import com.redmadrobot.flipper.ext.buildFeaturesMap

class LocalFlipperConfig: FlipperConfig {

    private val config = buildFeaturesMap {
        Features.Popular.details bind true
        Features.Popular.trailers bind true
        Features.Popular.actorDetails bind true

        Features.TvShow.tvShowDetails bind true
    }

    override fun getValue(feature: Feature): FlipperValue {
        return config[feature.id] ?: FlipperValue.EmptyValue
    }
}
