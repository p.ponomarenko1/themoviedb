package com.ponomarenko.themoviedb.feature.tvshows.presentation.list

import androidx.paging.PagingData
import com.ponomarenko.themoviedb.base.domain.lce.Lce
import com.ponomarenko.themoviedb.base.presentation.viewmodel.ViewState
import com.ponomarenko.themoviedb.feature.tvshows.domain.entity.TvShow

data class TvShowsViewState(
    val showsState: Lce<Unit> = Lce.Loading,
    val items: PagingData<TvShow> = PagingData.empty(),
    val isRefreshing: Boolean = false,
) : ViewState
