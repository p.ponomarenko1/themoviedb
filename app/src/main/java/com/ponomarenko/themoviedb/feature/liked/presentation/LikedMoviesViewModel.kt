package com.ponomarenko.themoviedb.feature.liked.presentation

import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.ponomarenko.themoviedb.base.presentation.viewmodel.StatefulViewModel
import com.ponomarenko.themoviedb.feature.liked.data.LikedMoviesRepository
import com.redmadrobot.extensions.lifecycle.mapDistinct
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class LikedMoviesViewModel @Inject constructor(
    private val likedRepository: LikedMoviesRepository
): StatefulViewModel<LikedMoviesViewState>() {

    val movies = liveState.mapDistinct { it.item }

     private fun fetchMovies() {
        likedRepository.getMoviesStream()
            .cachedIn(viewModelScope)
            .onEach {
                state = state.copy(item = it)
            }.launchIn(viewModelScope)
    }

    fun onResume() {
        state = LikedMoviesViewState(
            item = PagingData.empty()
        )
        fetchMovies()
    }

    fun onMovieClicked(id: Long) {
        navigateTo(LikedMoviesFragmentDirections.toDetailsMovieFragment(id))
    }
}
