package com.ponomarenko.themoviedb.feature.base.data

import com.ponomarenko.themoviedb.base.domain.lce.Lce
import com.ponomarenko.themoviedb.base.domain.lce.mapToLce
import com.ponomarenko.themoviedb.feature.actor.data.api.ActorApi
import com.ponomarenko.themoviedb.feature.actor.domain.entity.ActorDetails
import com.ponomarenko.themoviedb.feature.actor.domain.entity.ActorCredits
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Credits
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class MovieActorRepository @Inject constructor(
    private val api: ActorApi
) : ActorRepository {

    override fun getActorsStream(movieId: Long): Flow<Lce<Credits>> =
        flow {
            val credits = api.getActorsList(movieId = movieId).toDomain()
            emit(credits)
        }.mapToLce()

    override fun getDetailsStream(id: Int): Flow<Lce<ActorDetails>> =
        flow {
            val details = api.getPersonDetails(id).toDomain()
            emit(details)
        }.mapToLce()

    override fun getPersonCreditsStream(personId: Int): Flow<Lce<ActorCredits>> =
        flow {
            val movieCredits = api.getPersonMovieCredits(personId).toDomain()
            emit(movieCredits)
        }.mapToLce()
}
