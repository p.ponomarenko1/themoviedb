package com.ponomarenko.themoviedb.feature.tvshows.presentation.details

import androidx.lifecycle.viewModelScope
import com.ponomarenko.themoviedb.base.domain.extensions.EMPTY
import com.ponomarenko.themoviedb.base.domain.lce.Lce
import com.ponomarenko.themoviedb.base.presentation.viewmodel.StatefulViewModel
import com.ponomarenko.themoviedb.feature.tvshows.data.TvShowsRepository
import com.redmadrobot.extensions.lifecycle.mapDistinct
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class DetailsTvShowViewModel @AssistedInject constructor(
    @Assisted("id") private val id: Long,
    private val repository: TvShowsRepository
) : StatefulViewModel<DetailsTvShowViewState>() {

    val name = liveState.mapDistinct { it.name }
    val genre = liveState.mapDistinct { it.genre }
    val tagline = liveState.mapDistinct { it.tagline }
    val overview = liveState.mapDistinct { it.overview }
    val created = liveState.mapDistinct { it.created }
    val homepage = liveState.mapDistinct { it.homepage }
    val rating = liveState.mapDistinct { it.rating }
    val imageDetail = liveState.mapDistinct { it.imageDetail }

    init {
        state = DetailsTvShowViewState(
            name = String.EMPTY,
            genre = String.EMPTY,
            tagline = String.EMPTY,
            overview = String.EMPTY,
            created = String.EMPTY,
            homepage = String.EMPTY,
            rating = 0.0f,
            imageDetail = String.EMPTY,
            )
        getDetails()
    }

    private fun getDetails() {
        repository.getTvShowDetailsStream(id)
            .onEach { newState ->
                if (newState is Lce.Content)
                    with(newState.value)
                    {
                        state = state.copy(
                            name = name,
                            genre = genres.joinToString { it.name },
                            tagline = tagline,
                            overview = overview,
                            created = created.joinToString { it.name },
                            homepage = homepage,
                            rating = rating,
                            imageDetail = backdropPath
                        )
                    }
            }.launchIn(viewModelScope)
    }

    @AssistedFactory
    interface Factory {
        fun create(@Assisted("id") id: Long): DetailsTvShowViewModel
    }
}
