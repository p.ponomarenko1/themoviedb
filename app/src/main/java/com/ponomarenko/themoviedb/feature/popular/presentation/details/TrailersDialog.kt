package com.ponomarenko.themoviedb.feature.popular.presentation.details

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerSupportFragmentX
import com.ponomarenko.themoviedb.BuildConfig
import com.ponomarenko.themoviedb.R
import com.ponomarenko.themoviedb.app.utils.extentions.assistedViewModel
import com.ponomarenko.themoviedb.base.presentation.extensions.setupLinearLayoutManager
import com.ponomarenko.themoviedb.base.presentation.ui.dialog.BaseBottomSheetDialogFragment
import com.ponomarenko.themoviedb.databinding.DialogTrailersBinding
import com.redmadrobot.extensions.lifecycle.observe
import com.redmadrobot.extensions.viewbinding.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class TrailersDialog : BaseBottomSheetDialogFragment(R.layout.dialog_trailers) {

    @Inject
    lateinit var viewModeFactory: TrailersDialogViewModel.Factory
    private lateinit var youtubePlayer: YouTubePlayer

    private val args: TrailersDialogArgs by navArgs()
    private val viewModel: TrailersDialogViewModel by assistedViewModel {
        viewModeFactory.create(
            args.movieId
        )
    }
    private val binding: DialogTrailersBinding by viewBinding()

    private val epoxyController: TrailersEpoxyController by lazy {
        TrailersEpoxyController(
            onTrailerClicked = ::onTrailerClicked
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initBehavior()
        initRecycler()

        observe(viewModel.screenStateWithItems) { (state, items) ->
            epoxyController.setData(state, items)
        }
    }

    override val bottomSheetType = BottomSheetType.DEFAULT

    private fun initBehavior() {
        behavior.state = BottomSheetBehavior.STATE_EXPANDED
        behavior.skipCollapsed = true
    }

    private fun initRecycler() {
        with(binding.trailersList) {
            setController(epoxyController)
            setupLinearLayoutManager(orientation = RecyclerView.HORIZONTAL)
        }
    }

    private fun onTrailerClicked(key: String) {
        val youTubePlayerFragment: YouTubePlayerSupportFragmentX = YouTubePlayerSupportFragmentX.newInstance()
        youTubePlayerFragment.initialize(BuildConfig.YOUTUBE_API_KEY, object : YouTubePlayer.OnInitializedListener {
            override fun onInitializationSuccess(
                p0: YouTubePlayer.Provider?,
                player: YouTubePlayer?,
                wasRestored: Boolean
            ) {
                youtubePlayer = player!!
                youtubePlayer.fullscreenControlFlags = YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION
                youtubePlayer.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION)
                youtubePlayer.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_SYSTEM_UI)
                youtubePlayer.setFullscreen(false)
                if (!wasRestored) {
                    youtubePlayer.loadVideo(key)
                } else {
                    youtubePlayer.play()
                }
            }

            override fun onInitializationFailure(p0: YouTubePlayer.Provider?, p1: YouTubeInitializationResult?) = Unit
        })
        childFragmentManager.beginTransaction()
            .replace(R.id.youtube_player_fragment, youTubePlayerFragment).commit()
    }
}

