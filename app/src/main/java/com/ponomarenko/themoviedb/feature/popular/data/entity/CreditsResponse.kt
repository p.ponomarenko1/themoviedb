package com.ponomarenko.themoviedb.feature.popular.data.entity

import com.ponomarenko.themoviedb.base.data.api.NetworkEntity
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Cast
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Credits
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CreditsResponse(
    val id: Int,
    val cast: List<CastEntity>,
) : NetworkEntity<Credits> {
    override fun toDomain(): Credits {
        return Credits(
            id = id,
            cast = cast.map { it.toDomain() }
        )
    }
}

@JsonClass(generateAdapter = true)
data class CastEntity(
    val adult: Boolean,
    val gender: Int?,
    val id: Int,
    val known_for_department: String,
    val name: String,
    val original_name: String,
    val popularity: Float,
    val profile_path: String?,
    val cast_id: Int,
    val character: String,
    val credit_id: String,
    val order: Int,
) : NetworkEntity<Cast> {
    override fun toDomain(): Cast {
        return Cast(
            adult = adult,
            gender = gender,
            id = id,
            department = known_for_department,
            name = name,
            originalName = original_name,
            popularity = popularity,
            profilePath = profile_path,
            castId = cast_id,
            character = character,
            creditId = credit_id,
            order = order,
        )
    }
}
