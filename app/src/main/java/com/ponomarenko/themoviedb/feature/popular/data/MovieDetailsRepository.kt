package com.ponomarenko.themoviedb.feature.popular.data

import com.ponomarenko.themoviedb.base.domain.lce.Lce
import com.ponomarenko.themoviedb.base.domain.lce.mapToLce
import com.ponomarenko.themoviedb.feature.base.data.DetailsRepository
import com.ponomarenko.themoviedb.feature.base.data.MoviesDao
import com.ponomarenko.themoviedb.feature.base.data.entity.MovieDbEntity
import com.ponomarenko.themoviedb.feature.popular.data.api.PopularMoviesApi
import com.ponomarenko.themoviedb.feature.popular.domain.entity.MovieDetails
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class MovieDetailsRepository @Inject constructor(
    private val api: PopularMoviesApi,
    private val moviesDao: MoviesDao,
) : DetailsRepository<MovieDetails, MovieDbEntity> {

    override fun getDetails(movieId: Long): Flow<Lce<MovieDetails>> =
        flow {
            val details = api.getMovieDetails(movieId).toDomain()
            emit(details)
        }.mapToLce()

    override fun saveToFaw(item: MovieDbEntity): Flow<Lce<Unit>> =
        flow {
            val saveState = moviesDao.insert(item)
            emit(saveState)
        }.mapToLce()

    override fun delete(id: Long): Flow<Lce<Unit>> {
        return flow {
            val deleteState = moviesDao.delete(id)
            emit(deleteState)
        }.mapToLce()
    }

    fun isLiked(id: Long): Flow<Lce<Boolean>> =
        flow {
            val movie = moviesDao.getById(id)
            emit(movie?.id == id)
        }.mapToLce()
}
