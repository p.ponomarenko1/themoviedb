package com.ponomarenko.themoviedb.feature.actor.domain.entity

data class CastPerson(
    val character: String,
    val creditId: String,
    val releaseDate: String,
    val voteCount: Int,
    val video: Boolean,
    val adult: Boolean,
    val voteAverage: Float,
    val title: String,
    val genreIds: List<Int>,
    val originalLanguage: String,
    val originalTitle: String,
    val popularity: Float,
    val id: Long,
    val backdropPath: String?,
    val overview: String,
    val posterPath: String?
)
