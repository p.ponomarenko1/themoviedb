package com.ponomarenko.themoviedb.feature.actor.data.api

import com.ponomarenko.themoviedb.feature.actor.data.entity.ActorDetailsResponse
import com.ponomarenko.themoviedb.feature.actor.data.entity.MovieCreditsResponse
import com.ponomarenko.themoviedb.feature.popular.data.entity.CreditsResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface ActorApi {

    @GET("movie/{movie_id}/credits")
    suspend fun getActorsList(@Path("movie_id") movieId: Long): CreditsResponse

    @GET("person/{person_id}")
    suspend fun getPersonDetails(@Path("person_id") personId: Int): ActorDetailsResponse

    @GET("person/{person_id}/movie_credits")
    suspend fun getPersonMovieCredits(@Path("person_id") personId: Int): MovieCreditsResponse
}

