package com.ponomarenko.themoviedb.feature.base.data

import androidx.paging.PagingData
import com.ponomarenko.themoviedb.base.domain.lce.Lce
import kotlinx.coroutines.flow.Flow

interface MoviesRepository<T : Any> {
    fun getMoviesStream(page: Int = 1): Flow<PagingData<T>>
}
