package com.ponomarenko.themoviedb.feature.popular.presentation.list

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.PagingData
import com.ponomarenko.themoviedb.R
import com.ponomarenko.themoviedb.base.presentation.extensions.setupLinearLayoutManager
import com.ponomarenko.themoviedb.base.presentation.ui.BaseFragment
import com.ponomarenko.themoviedb.databinding.FragmentTopMoviesBinding
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Movie
import com.ponomarenko.themoviedb.feature.popular.presentation.PagingMoviesEpoxyController
import com.ponomarenko.ui.extensions.resolveColor
import com.redmadrobot.extensions.lifecycle.observe
import com.redmadrobot.extensions.resources.getDimension
import com.redmadrobot.extensions.viewbinding.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ObsoleteCoroutinesApi

@ObsoleteCoroutinesApi
@AndroidEntryPoint
class PopularMoviesFragment : BaseFragment(R.layout.fragment_top_movies) {

    private val viewModel: PopularMoviesViewModel by viewModels()
    private val binding: FragmentTopMoviesBinding by viewBinding()

    private val epoxyController by lazy {
        PagingMoviesEpoxyController(
            onMovieClicked = viewModel::onMovieClicked
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        observe(viewModel.items, ::renderItems)
        observe(viewModel.isRefreshing, binding.moviesSwipeRefresh::setRefreshing)
        observe(viewModel.events, ::onEvent)
    }

    private fun initUi() = with(binding) {
        with(moviesEpoxyList) {
            setupLinearLayoutManager()
            setController(epoxyController)
        }
        with(moviesSwipeRefresh) {
            setProgressViewEndTarget(true, getDimension(R.dimen.movie_swipe_refresh_target).toInt())
            setColorSchemeColors(resolveColor(R.attr.colorPrimary))
            setOnRefreshListener { viewModel.onSwipeRefresh() }
        }
    }

    private fun renderItems(items: PagingData<Movie>?) {
        if (items != null) {
            lifecycleScope.launchWhenStarted { epoxyController.submitData(items) }
        }
    }
}
