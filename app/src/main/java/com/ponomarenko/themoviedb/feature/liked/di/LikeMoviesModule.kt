package com.ponomarenko.themoviedb.feature.liked.di

import com.ponomarenko.themoviedb.app.data.AppDatabase
import com.ponomarenko.themoviedb.feature.base.data.MoviesDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@Module
@InstallIn(ActivityRetainedComponent::class)
abstract class LikeMoviesModule {


    companion object {
        @Provides
        fun provideTopMoviesApi(appDatabase: AppDatabase): MoviesDao = appDatabase.moviesDao
    }
}
