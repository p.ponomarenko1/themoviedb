package com.ponomarenko.themoviedb.feature.actor.data.entity

import com.ponomarenko.themoviedb.base.data.api.NetworkEntity
import com.ponomarenko.themoviedb.feature.actor.domain.entity.CastPerson
import com.ponomarenko.themoviedb.feature.actor.domain.entity.Crew
import com.ponomarenko.themoviedb.feature.actor.domain.entity.ActorCredits
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MovieCreditsResponse(
    val cast: List<CastPersonEntity>,
    val crew: List<CrewEntity>,
    val id: Int
) : NetworkEntity<ActorCredits> {
    override fun toDomain(): ActorCredits {
        return ActorCredits(
            cast = cast.map { it.toDomain() },
            crew = crew.map { it.toDomain() },
            id = id
        )
    }
}

@JsonClass(generateAdapter = true)
data class CastPersonEntity(
    val character: String,
    val credit_id: String,
    val release_date: String,
    val vote_count: Int,
    val video: Boolean,
    val adult: Boolean,
    val vote_average: Float,
    val title: String,
    val genre_ids: List<Int>,
    val original_language: String,
    val original_title: String,
    val popularity: Float,
    val id: Long,
    val backdrop_path: String?,
    val overview: String,
    val poster_path: String?
) : NetworkEntity<CastPerson> {
    override fun toDomain(): CastPerson {
        return CastPerson(
            character = character,
            creditId = credit_id,
            releaseDate = release_date,
            voteCount = vote_count,
            video = video,
            adult = adult,
            voteAverage = vote_average,
            title = title,
            genreIds = genre_ids,
            originalLanguage = original_language,
            originalTitle = original_title,
            popularity = popularity,
            id = id,
            backdropPath = backdrop_path,
            overview = overview,
            posterPath = poster_path
        )
    }
}

@JsonClass(generateAdapter = true)
data class CrewEntity(
    val id: Int,
    val department: String,
    val original_language: String,
    val original_title: String,
    val job: String,
    val overview: String,
    val vote_count: Int,
    val video: Boolean,
    val poster_path: String?,
    val backdrop_path: String?,
    val title: String,
    val popularity: Float,
    val genre_ids: List<Int>,
    val vote_average: Float,
    val adult: Boolean,
    val release_date: String,
    val credit_id: String,
) : NetworkEntity<Crew> {
    override fun toDomain(): Crew {
        return Crew(
            id = id,
            department = department,
            originalLanguage = original_language,
            originalTitle = original_title,
            job = job,
            overview = overview,
            voteCount = vote_count,
            video = video,
            posterPath = poster_path,
            backdropPath = backdrop_path,
            title = title,
            popularity = popularity,
            genreIds = genre_ids,
            voteAverage = vote_average,
            adult = adult,
            releaseDate = release_date,
            creditId = credit_id
        )
    }
}
