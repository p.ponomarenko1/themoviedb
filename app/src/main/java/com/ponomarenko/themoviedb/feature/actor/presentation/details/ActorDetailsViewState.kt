package com.ponomarenko.themoviedb.feature.actor.presentation.details

import com.ponomarenko.themoviedb.base.domain.lce.Lce
import com.ponomarenko.themoviedb.base.presentation.viewmodel.ViewState
import com.ponomarenko.themoviedb.feature.actor.domain.entity.CastPerson
import com.ponomarenko.themoviedb.feature.actor.domain.entity.Crew

data class ActorDetailsViewState(
    val birthday: String?,
    val name: String,
    val knownAs: String,
    val biography: String,
    val popularity: Float,
    val placeOfBirth: String?,
    val profilePath: String?,
    val screenState: Lce<Unit> = Lce.Loading,
    val cast: List<CastPerson> = emptyList()
) : ViewState
