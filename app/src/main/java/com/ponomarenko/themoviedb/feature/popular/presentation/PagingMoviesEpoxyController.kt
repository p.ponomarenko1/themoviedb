package com.ponomarenko.themoviedb.feature.popular.presentation

import androidx.paging.LoadState
import androidx.paging.LoadStates
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.paging3.PagingDataEpoxyController
import com.ponomarenko.themoviedb.R
import com.ponomarenko.themoviedb.base.domain.extensions.reduceStateKeepNotLoading
import com.ponomarenko.themoviedb.base.presentation.entity.Paddings
import com.ponomarenko.themoviedb.base.presentation.entity.dp
import com.ponomarenko.themoviedb.base.presentation.entity.pagingLoadStates
import com.ponomarenko.themoviedb.base.presentation.ui.view.*
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Movie
import kotlinx.coroutines.ObsoleteCoroutinesApi

@ObsoleteCoroutinesApi
class PagingMoviesEpoxyController(
    private val onMovieClicked: (movieId: Long) -> Unit
) : PagingDataEpoxyController<Movie>() {

    companion object {
        private const val SHIMMERS_COUNT = 6
        private const val PAGING_ERROR_PADDING_BOTTOM_DP = 24
    }

    private val loadStates by pagingLoadStates { oldStates, newStates ->
        LoadStates(
            refresh = reduceStateKeepNotLoading(oldStates.refresh, newStates.refresh),
            prepend = newStates.prepend,
            append = newStates.append,
        )
    }

    init {
        requestModelBuild()
    }

    override fun buildItemModel(currentPosition: Int, item: Movie?): EpoxyModel<*> {
        checkNotNull(item)
        return RectangleImageTextCellModel_()
            .id(item.id)
            .title(item.title)
            .image(item.posterPath)
            .description(item.overview ?: "")
            .rating(item.rating)
            .release(R.string.release_date, item.releaseDate)
            .onClickListener { onMovieClicked(item.id) }
    }

    override fun addModels(models: List<EpoxyModel<*>>) {
        when (loadStates.refresh) {
            LoadState.Loading -> layoutShimmers(
                R.layout.shimmer_image_text_cell,
                false,
                SHIMMERS_COUNT
            )

            is LoadState.Error -> buildError()

            is LoadState.NotLoading -> {
                if (models.isEmpty()) {
                    buildNotFind()
                } else {
                    super.addModels(models)
                }
            }
        }

        when (loadStates.append) {
            LoadState.Loading -> layoutShimmers(R.layout.shimmer_image_text_cell)
            is LoadState.Error -> buildPaginationError()
            is LoadState.NotLoading -> Unit
        }
    }

    private fun buildError() {
        stubView {
            id("error")
            image(R.drawable.image_connection_error)
            title(R.string.connection_error_title)
            text(R.string.connection_error_text)
            action(StubView.Action(R.string.connection_try_again) { retry() })
        }
    }

    private fun buildNotFind() {
        stubView {
            id("noFound")
            image(R.drawable.image_error_loading)
            title(R.string.no_found)
        }
    }

    private fun buildPaginationError() {
        paginationErrorCell(
            text = R.string.data_loading_error,
            paddings = Paddings(bottom = PAGING_ERROR_PADDING_BOTTOM_DP.dp())
        )
    }
}
