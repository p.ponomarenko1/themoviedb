package com.ponomarenko.themoviedb.feature.tvshows.domain.entity

data class TvShow(
    val posterPath: String?,
    val popularity: Float,
    val id: Long,
    val backdropPath: String?,
    val voteAverage: Float,
    val overview: String,
    val firstAirDate: String?,
    val originCountry: List<String>,
    val genreIds: List<Int>,
    val originalLanguage: String,
    val voteCount: Int,
    val name: String,
    val originalName: String
) {
    val rating: Float
    get() = voteAverage.div(2)
}
