package com.ponomarenko.themoviedb.feature.popular.presentation.details

import com.airbnb.epoxy.Typed2EpoxyController
import com.ponomarenko.themoviedb.R
import com.ponomarenko.themoviedb.base.domain.lce.Lce
import com.ponomarenko.themoviedb.base.presentation.ui.view.imageTextCell
import com.ponomarenko.themoviedb.base.presentation.ui.view.layoutShimmers
import com.ponomarenko.themoviedb.base.presentation.ui.view.stubView
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Trailer

class TrailersEpoxyController(
    private val onTrailerClicked: (String) -> Unit,
) : Typed2EpoxyController<Lce<Unit>, List<Trailer>>() {


    override fun buildModels(state: Lce<Unit>, items: List<Trailer>) {
        when (state) {
            is Lce.Loading -> {
                layoutShimmers(R.layout.shimmer_image_one_text_cell, true, TRAILER_SHIMMERS_COUNT)
            }

            is Lce.Content -> {
                if (items.isEmpty()) {
                    stubView
                    return
                }
                for (entity in items) {
                    imageTextCell {
                        id(entity.id)
                        title(entity.name)
                        image(entity.imagePath)
                        onClickListener { onTrailerClicked.invoke(entity.key) }
                    }
                }
            }
            is Lce.Error -> stubView
        }
    }

    private val stubView
        get() = stubView {
            id("stub_error")
            image(R.drawable.image_error_loading)
            title(R.string.trailers_error)
            text(R.string.data_loading_error)
        }
}

private const val TRAILER_SHIMMERS_COUNT = 6
