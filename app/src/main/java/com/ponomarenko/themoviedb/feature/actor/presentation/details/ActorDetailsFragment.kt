package com.ponomarenko.themoviedb.feature.actor.presentation.details

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import com.ponomarenko.themoviedb.R
import com.ponomarenko.themoviedb.app.utils.extentions.assistedViewModel
import com.ponomarenko.themoviedb.base.presentation.extensions.loadImage
import com.ponomarenko.themoviedb.base.presentation.extensions.setupLinearLayoutManager
import com.ponomarenko.themoviedb.base.presentation.ui.BaseFragment
import com.ponomarenko.themoviedb.databinding.FragmentActorDetailsBinding
import com.redmadrobot.extensions.lifecycle.observe
import com.redmadrobot.extensions.viewbinding.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ActorDetailsFragment : BaseFragment(R.layout.fragment_actor_details) {

    @Inject
    lateinit var viewModelFactory: ActorDetailsViewModel.Factory

    private val args: ActorDetailsFragmentArgs by navArgs()
    private val binding: FragmentActorDetailsBinding by viewBinding()
    private val viewModel: ActorDetailsViewModel by assistedViewModel {
        viewModelFactory.create(args.actorId)
    }

    private val epoxyController by lazy {
        CastEpoxyController(
            onMovieClicked = viewModel::onMovieClicked
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView()

        observe(viewModel.events, ::onEvent)
        observe(viewModel.profilePath, binding.imagePreview::loadImage)
        observe(viewModel.name, binding.nameTv::setText)
        observe(viewModel.knownAs, binding.alsoNameTv::setText)
        observe(viewModel.birthday, binding.birthdayTv::setText)
        observe(viewModel.placeOfBirth, binding.placeTv::setText)
        observe(viewModel.biography, binding.descriptionTv::setText)
        observe(viewModel.popularity, binding.rating::setRating)
        observe(viewModel.screenStateWithItems) { (state, items) ->
            epoxyController.setData(state, items)
        }
    }

    private fun initRecyclerView() {
        with(binding.recyclerView) {
            setController(epoxyController)
            setupLinearLayoutManager(orientation = RecyclerView.HORIZONTAL)
        }
    }

}
