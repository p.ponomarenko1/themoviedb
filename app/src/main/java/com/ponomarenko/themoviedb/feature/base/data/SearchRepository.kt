package com.ponomarenko.themoviedb.feature.base.data

import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow

interface SearchRepository<T : Any> {
    fun searchStream(query: String, page: Int = 1): Flow<PagingData<T>>
}
