package com.ponomarenko.themoviedb.feature.popular.presentation.details

import com.ponomarenko.themoviedb.base.domain.lce.Lce
import com.ponomarenko.themoviedb.base.presentation.viewmodel.ViewState
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Cast

data class DetailsMovieViewState(
    val id: Long,
    val adult: Boolean,
    val posterPath: String?,
    val title: String,
    val originalTitle: String,
    val originalLanguage: String,
    val overview: String?,
    val voteAverage: Float,
    val productionCompanies: CharSequence,
    val genres: CharSequence,
    val releaseDate: String,
    val screenState: Lce<Unit> = Lce.Loading,
    val credits: List<Cast> = emptyList(),
    val isLiked: Boolean
) : ViewState
