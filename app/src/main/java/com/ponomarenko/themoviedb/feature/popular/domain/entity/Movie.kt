package com.ponomarenko.themoviedb.feature.popular.domain.entity

data class Movie(
    val posterPath: String?,
    val adult: Boolean,
    val overview: String?,
    val genreIds: List<Int>,
    val id: Long,
    val originalTitle: String,
    val originalLanguage: String,
    val title: String,
    val backdropPath: String?,
    val popularity: Double,
    val releaseDate: String?,
    val voteAverage: Double
) {
    val rating: Float
        get() = voteAverage.div(2).toFloat()
}
