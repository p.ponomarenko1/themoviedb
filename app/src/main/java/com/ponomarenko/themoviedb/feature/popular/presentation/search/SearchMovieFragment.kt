package com.ponomarenko.themoviedb.feature.popular.presentation.search

import android.os.Bundle
import android.view.View
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.PagingData
import com.ponomarenko.themoviedb.R
import com.ponomarenko.themoviedb.base.presentation.extensions.setupLinearLayoutManager
import com.ponomarenko.themoviedb.base.presentation.ui.BaseFragment
import com.ponomarenko.themoviedb.databinding.FragmentSearchMovieBinding
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Movie
import com.ponomarenko.themoviedb.feature.popular.presentation.PagingMoviesEpoxyController
import com.redmadrobot.extensions.lifecycle.observe
import com.redmadrobot.extensions.viewbinding.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ObsoleteCoroutinesApi

@ObsoleteCoroutinesApi
@AndroidEntryPoint
class SearchMovieFragment : BaseFragment(R.layout.fragment_search_movie) {

    private val binding: FragmentSearchMovieBinding by viewBinding()
    private val viewModel: SearchMovieViewModel by viewModels()

    private val epoxyController by lazy {
        PagingMoviesEpoxyController(
            onMovieClicked = viewModel::onMovieClicked
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initSearchInput()
        iniRecycler()

        observe(viewModel.items, ::renderItems)
        observe(viewModel.events, ::onEvent)
    }

    private fun initSearchInput() {
        binding.findMovieSearchInput.addTextChangedListener(
            onTextChanged = { text, _, _, _ ->
                viewModel.onQueryEdited(text)
            }
        )
    }

    private fun iniRecycler() = with(binding.findMovieList) {
        setupLinearLayoutManager()
        setController(epoxyController)
    }

    private fun renderItems(items: PagingData<Movie>?) {
        if (items != null) {
            lifecycleScope.launchWhenStarted { epoxyController.submitData(items) }
        }
    }
}
