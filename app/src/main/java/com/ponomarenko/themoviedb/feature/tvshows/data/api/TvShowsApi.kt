package com.ponomarenko.themoviedb.feature.tvshows.data.api

import com.ponomarenko.themoviedb.feature.tvshows.data.entity.TvShowDetailsResponse
import com.ponomarenko.themoviedb.feature.tvshows.data.entity.TvShowsResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TvShowsApi {

    @GET("tv/popular")
    suspend fun getPopularTvShows(@Query("page") page: Int): TvShowsResponse

    @GET("tv/{tv_id}")
    suspend fun getTvShowDetails(@Path("tv_id") id: Long): TvShowDetailsResponse
}
