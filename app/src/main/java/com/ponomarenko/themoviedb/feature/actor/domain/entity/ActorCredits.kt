package com.ponomarenko.themoviedb.feature.actor.domain.entity

data class ActorCredits(
    val cast: List<CastPerson>,
    val crew: List<Crew>,
    val id: Int,
)
