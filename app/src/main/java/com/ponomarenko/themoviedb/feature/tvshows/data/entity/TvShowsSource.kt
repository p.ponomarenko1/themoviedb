package com.ponomarenko.themoviedb.feature.tvshows.data.entity

import androidx.paging.PagingSource
import com.ponomarenko.themoviedb.feature.tvshows.data.api.TvShowsApi
import com.ponomarenko.themoviedb.feature.tvshows.domain.entity.TvShow
import retrofit2.HttpException
import java.io.IOException
import java.net.HttpURLConnection

class TvShowsSource(
    private val api: TvShowsApi
) : PagingSource<Int, TvShow>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, TvShow> {
        return try {
            val nextPage = params.key ?: 1
            val response = api.getPopularTvShows(nextPage).toDomain()

            LoadResult.Page(
                data = response.result,
                prevKey = null,
                nextKey = response.page + 1
            )
        } catch (e: IOException) {
            LoadResult.Error(e)
        } catch (e: HttpException) {
            if (e.code() == HttpURLConnection.HTTP_NOT_FOUND) {
                LoadResult.Page(data = emptyList(), prevKey = null, nextKey = null)
            } else {
                LoadResult.Error(e)
            }
        }
    }
}
