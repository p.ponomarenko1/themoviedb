package com.ponomarenko.themoviedb.feature.liked.data

import androidx.paging.PagingData
import com.ponomarenko.themoviedb.base.domain.extensions.emulatePaging
import com.ponomarenko.themoviedb.feature.base.data.MoviesDao
import com.ponomarenko.themoviedb.feature.base.data.MoviesRepository
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Movie
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class LikedMoviesRepository @Inject constructor(
    private val moviesDao: MoviesDao,
) : MoviesRepository<Movie> {

    override fun getMoviesStream(page: Int): Flow<PagingData<Movie>> =
        emulatePaging {
            val movies = moviesDao.getAll().map { it.toDomain() }
            movies
        }
}
