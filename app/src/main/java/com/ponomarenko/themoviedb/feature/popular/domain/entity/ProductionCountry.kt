package com.ponomarenko.themoviedb.feature.popular.domain.entity

data class ProductionCountry(
    val iso: String,
    val name: String
)
