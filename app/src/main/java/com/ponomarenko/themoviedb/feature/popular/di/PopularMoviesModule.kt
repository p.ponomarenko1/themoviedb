package com.ponomarenko.themoviedb.feature.popular.di

import com.ponomarenko.themoviedb.feature.popular.data.api.PopularMoviesApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import retrofit2.Retrofit
import retrofit2.create

@Module
@InstallIn(ActivityRetainedComponent::class)
abstract class PopularMoviesModule {

    companion object {
        @Provides
        @ActivityRetainedScoped
        fun provideTopMoviesApi(retrofit: Retrofit): PopularMoviesApi = retrofit.create()
    }
}
