package com.ponomarenko.themoviedb.feature.tvshows.data

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.ponomarenko.themoviedb.base.domain.lce.Lce
import com.ponomarenko.themoviedb.base.domain.lce.mapToLce
import com.ponomarenko.themoviedb.feature.tvshows.data.api.TvShowsApi
import com.ponomarenko.themoviedb.feature.tvshows.data.entity.TvShowsSource
import com.ponomarenko.themoviedb.feature.tvshows.domain.entity.TvShow
import com.ponomarenko.themoviedb.feature.tvshows.domain.entity.TvShowDetails
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class TvShowsRepository @Inject constructor(
    private val api: TvShowsApi
) {

    fun getPopularTvShowsStream(pageSize: Int = 1): Flow<PagingData<TvShow>> {
        return Pager(
            config = PagingConfig(
                pageSize = pageSize,
                initialLoadSize = pageSize + 1,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { TvShowsSource(api) }
        ).flow
    }

    fun getTvShowDetailsStream(id: Long): Flow<Lce<TvShowDetails>> {
        return flow {
            val details = api.getTvShowDetails(id = id).toDomain()
            emit(details)
        }.mapToLce()
    }
}
