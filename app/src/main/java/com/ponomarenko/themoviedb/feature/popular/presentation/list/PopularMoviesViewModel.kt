package com.ponomarenko.themoviedb.feature.popular.presentation.list

import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.ponomarenko.themoviedb.base.presentation.viewmodel.StatefulViewModel
import com.ponomarenko.themoviedb.feature.popular.data.PopularMoviesRepository
import com.ponomarenko.themoviedb.feature.toggles.Features
import com.redmadrobot.extensions.lifecycle.mapDistinct
import com.redmadrobot.flipper.ext.flipperPoint
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class PopularMoviesViewModel @Inject constructor(
    private val popularMoviesRepository: PopularMoviesRepository,
) : StatefulViewModel<PopularMoviesViewState>() {

    val items = liveState.mapDistinct { it.item }
    val isRefreshing = liveState.mapDistinct { it.isRefreshing }

    init {
        state = PopularMoviesViewState(
            item = PagingData.empty(),
            isRefreshing = false
        )
        fetchMovies()
    }

    private fun fetchMovies() {
        popularMoviesRepository.getMoviesStream()
            .cachedIn(viewModelScope)
            .onEach { newState ->
                state = state.copy(
                    item = newState,
                    isRefreshing = false
                )
            }
            .launchIn(viewModelScope)
    }

    fun onSwipeRefresh() {
        state = state.copy(isRefreshing = true)
        fetchMovies()
    }

    fun onMovieClicked(movieId: Long) {
        flipperPoint(Features.Popular.details) {
            navigateTo(
                PopularMoviesFragmentDirections.toDetailsMovie(movieId = movieId),
                rootGraph = true
            )
        }
    }
}
