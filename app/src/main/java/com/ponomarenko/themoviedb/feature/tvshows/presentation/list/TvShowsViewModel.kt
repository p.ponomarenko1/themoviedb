package com.ponomarenko.themoviedb.feature.tvshows.presentation.list

import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.ponomarenko.themoviedb.base.domain.lce.Lce
import com.ponomarenko.themoviedb.base.presentation.viewmodel.StatefulViewModel
import com.ponomarenko.themoviedb.feature.toggles.Features
import com.ponomarenko.themoviedb.feature.tvshows.data.TvShowsRepository
import com.redmadrobot.extensions.lifecycle.mapDistinct
import com.redmadrobot.flipper.ext.flipperPoint
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class TvShowsViewModel @Inject constructor(
    private val repository: TvShowsRepository
) : StatefulViewModel<TvShowsViewState>() {

    val items = liveState.mapDistinct { it.items }
    val isRefreshing = liveState.mapDistinct { it.isRefreshing }

    init {
        state = TvShowsViewState(
            showsState = Lce.Loading,
            items = PagingData.empty()
        )
        fetchTvShows()
    }

    private fun fetchTvShows() {
        repository.getPopularTvShowsStream()
            .cachedIn(viewModelScope)
            .onEach { newState ->
                state = state.copy(
                    items = newState,
                    isRefreshing = false
                )
            }.launchIn(viewModelScope)
    }

    fun onSwipeRefresh() {
        state = state.copy(isRefreshing = true)
        fetchTvShows()
    }

    fun onTvShowClicked(id: Long) {
        flipperPoint(Features.TvShow.tvShowDetails) {
            navigateTo(TvShowsFragmentDirections.toDetailsTvShowFragment(id = id))
        }
    }
}
