package com.ponomarenko.themoviedb.feature.tvshows.presentation.list

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.PagingData
import com.ponomarenko.themoviedb.R
import com.ponomarenko.themoviedb.base.presentation.extensions.setupLinearLayoutManager
import com.ponomarenko.themoviedb.base.presentation.ui.BaseFragment
import com.ponomarenko.themoviedb.databinding.FragmentTvShowsBinding
import com.ponomarenko.themoviedb.feature.tvshows.domain.entity.TvShow
import com.ponomarenko.ui.extensions.resolveColor
import com.redmadrobot.extensions.lifecycle.observe
import com.redmadrobot.extensions.resources.getDimension
import com.redmadrobot.extensions.viewbinding.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ObsoleteCoroutinesApi

@ObsoleteCoroutinesApi
@AndroidEntryPoint
class TvShowsFragment : BaseFragment(R.layout.fragment_tv_shows) {

    private val viewModel: TvShowsViewModel by viewModels()
    private val binding: FragmentTvShowsBinding by viewBinding()

    private val epoxyController by lazy {
        TvShowEpoxyController(
            onTvShowClicked = viewModel::onTvShowClicked
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        observe(viewModel.isRefreshing, binding.showsSwipeRefresh::setRefreshing)
        observe(viewModel.items, ::renderItems)
        observe(viewModel.events, ::onEvent)
    }

    private fun initUi() = with(binding) {
        with(showsEpoxyList) {
            setupLinearLayoutManager()
            setController(epoxyController)
        }
        with(showsSwipeRefresh) {
            setProgressViewEndTarget(true, getDimension(R.dimen.movie_swipe_refresh_target).toInt())
            setColorSchemeColors(resolveColor(R.attr.colorPrimary))
            setOnRefreshListener { viewModel.onSwipeRefresh() }
        }
    }

    private fun renderItems(items: PagingData<TvShow>?) {
        if (items != null) {
            lifecycleScope.launchWhenStarted { epoxyController.submitData(items) }
        }
    }
}
