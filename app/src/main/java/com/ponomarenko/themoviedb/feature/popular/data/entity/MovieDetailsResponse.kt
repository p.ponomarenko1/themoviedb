package com.ponomarenko.themoviedb.feature.popular.data.entity

import com.ponomarenko.themoviedb.base.data.api.NetworkEntity
import com.ponomarenko.themoviedb.feature.popular.domain.entity.*
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MovieDetailsResponse(
    val adult: Boolean,
    val backdrop_path: String?,
    val belongs_to_collection: Any?,
    val budget: Int,
    val genres: List<GenreEntity>,
    val homepage: String?,
    val id: Long,
    val imdb_id: String?,
    val original_language: String,
    val original_title: String,
    val overview: String?,
    val popularity: Double,
    val poster_path: String?,
    val production_companies: List<ProductionCompanyEntity>,
    val production_countries: List<ProductionCountryEntity>,
    val release_date: String,
    val revenue: Int,
    val runtime: Int?,
    val spoken_languages: List<SpokenLanguageEntity>,
    val status: String,
    val tagline: String?,
    val title: String,
    val video: Boolean,
    val vote_average: Double,
    val vote_count: Int
) : NetworkEntity<MovieDetails> {
    override fun toDomain(): MovieDetails {
        return MovieDetails(
            adult = adult,
            backdropPath = backdrop_path,
            belongsToCollection = belongs_to_collection,
            budget = budget,
            genres = genres.map { it.toDomain() },
            homepage = homepage,
            id = id,
            imdbId = imdb_id,
            originalLanguage = original_language,
            originalTitle = original_title,
            overview = overview,
            popularity = popularity,
            posterPath = poster_path,
            productionCompanies = production_companies.map { it.toDomain() },
            productionCountries = production_countries.map { it.toDomain() },
            releaseDate = release_date,
            revenue = revenue,
            runtime = runtime,
            spokenLanguages = spoken_languages.map { it.toDomain() },
            status = status,
            tagline = tagline,
            title = title,
            video = video,
            voteAverage = vote_average,
            voteCount = vote_count
        )
    }
}

@JsonClass(generateAdapter = true)
data class GenreEntity(
    val id: Int,
    val name: String
) : NetworkEntity<Genre> {
    override fun toDomain(): Genre {
        return Genre(
            id = id,
            name = name
        )
    }
}

@JsonClass(generateAdapter = true)
data class ProductionCompanyEntity(
    val id: Int,
    val logo_path: String?,
    val name: String,
    val origin_country: String
) : NetworkEntity<ProductionCompany> {
    override fun toDomain(): ProductionCompany {
       return ProductionCompany(
           id = id,
           logoPath = logo_path,
           name = name,
           originCountry = origin_country
       )
    }
}

@JsonClass(generateAdapter = true)
data class ProductionCountryEntity(
    val iso_3166_1: String,
    val name: String
) : NetworkEntity<ProductionCountry> {
    override fun toDomain(): ProductionCountry {
        return ProductionCountry(
            iso = iso_3166_1,
            name = name
        )
    }
}

@JsonClass(generateAdapter = true)
data class SpokenLanguageEntity(
    val iso_639_1: String,
    val name: String
) : NetworkEntity<SpokenLanguage> {
    override fun toDomain(): SpokenLanguage {
        return SpokenLanguage(
            iso = iso_639_1,
            name = name
        )
    }
}
