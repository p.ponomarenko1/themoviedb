package com.ponomarenko.themoviedb.feature.tvshows.domain.entity

data class TvShowsDomainEntity(
    val page: Int,
    val result: List<TvShow>,
    val totalPages: Int,
    val totalResults: Int
)
