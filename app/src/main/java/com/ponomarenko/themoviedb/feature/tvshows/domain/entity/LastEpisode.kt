package com.ponomarenko.themoviedb.feature.tvshows.domain.entity

data class LastEpisode(
    val airDate: String?,
    val episodeNumber: Int,
    val name: String,
    val overview: String,
    val productionCode: String,
    val seasonNumber: Int,
    val stillPath: String?,
    val voteAverage: Float,
    val voteCount: Int
)
