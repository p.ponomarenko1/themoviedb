package com.ponomarenko.themoviedb.feature.liked.presentation

import androidx.paging.PagingData
import com.ponomarenko.themoviedb.feature.base.presentation.MoviesViewState
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Movie

data class LikedMoviesViewState(
    override val item: PagingData<Movie>,
) : MoviesViewState<Movie>
