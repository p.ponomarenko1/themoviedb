package com.ponomarenko.themoviedb.feature.tvshows.domain.entity

data class GenreTvShow(
    val id: Int,
    val name: String
)
