package com.ponomarenko.themoviedb.feature.actor.di

import com.ponomarenko.themoviedb.feature.actor.data.api.ActorApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import retrofit2.Retrofit
import retrofit2.create

@Module
@InstallIn(ActivityRetainedComponent::class)
abstract class ActorModule {
    companion object {
        @Provides
        @ActivityRetainedScoped
        fun provideActorApi(retrofit: Retrofit): ActorApi = retrofit.create()
    }
}

