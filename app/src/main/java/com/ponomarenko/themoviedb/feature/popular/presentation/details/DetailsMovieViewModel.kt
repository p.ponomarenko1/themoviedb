package com.ponomarenko.themoviedb.feature.popular.presentation.details

import androidx.lifecycle.viewModelScope
import com.ponomarenko.themoviedb.R
import com.ponomarenko.themoviedb.base.domain.extensions.EMPTY
import com.ponomarenko.themoviedb.base.domain.lce.Lce
import com.ponomarenko.themoviedb.base.presentation.viewmodel.StatefulViewModel
import com.ponomarenko.themoviedb.feature.base.data.MovieActorRepository
import com.ponomarenko.themoviedb.feature.base.data.entity.MovieDbEntity
import com.ponomarenko.themoviedb.feature.popular.data.MovieDetailsRepository
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Credits
import com.ponomarenko.themoviedb.feature.popular.domain.entity.MovieDetails
import com.ponomarenko.themoviedb.feature.toggles.Features
import com.redmadrobot.extensions.lifecycle.mapDistinct
import com.redmadrobot.flipper.ext.flipperPoint
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.zip
import timber.log.Timber

class DetailsMovieViewModel @AssistedInject constructor(
    @Assisted("movieId") private val movieId: Long,
    private val moviesRepository: MovieDetailsRepository,
    private val actorRepository: MovieActorRepository,
) : StatefulViewModel<DetailsMovieViewState>() {

    val posterPath = liveState.mapDistinct { it.posterPath }
    val title = liveState.mapDistinct { it.title }
    val overview = liveState.mapDistinct { it.overview }
    val voteAverage = liveState.mapDistinct { it.voteAverage }
    val productionCompanies = liveState.mapDistinct { it.productionCompanies }
    val genres = liveState.mapDistinct { it.genres }
    val releaseDate = liveState.mapDistinct { it.releaseDate }
    val isLiked = liveState.mapDistinct { it.isLiked }

    val screenStateWithItems = liveState.mapDistinct { it.screenState to it.credits }

    init {
        state = DetailsMovieViewState(
            posterPath = String.EMPTY,
            title = String.EMPTY,
            overview = String.EMPTY,
            voteAverage = 0f,
            productionCompanies = String.EMPTY,
            genres = String.EMPTY,
            releaseDate = String.EMPTY,
            id = movieId,
            adult = false,
            originalLanguage = String.EMPTY,
            originalTitle = String.EMPTY,
            isLiked = false
        )
        fetchDetails()
        isLiked()
    }

    private fun fetchDetails() {

        moviesRepository.getDetails(movieId = movieId)
            .zip(actorRepository.getActorsStream(movieId = movieId), ::Pair)
            .onEach { (movieDetails, actors) ->
                handleDetailsState(movieDetails)
                handleActorsState(actors)
            }
            .launchIn(viewModelScope)
    }

    private fun handleDetailsState(detailsState: Lce<MovieDetails>) {
        when (detailsState) {
            is Lce.Loading -> Unit
            is Lce.Content -> {
                with(detailsState.value)
                {
                    state = state.copy(
                        adult = adult,
                        originalTitle = originalTitle,
                        originalLanguage = originalLanguage,
                        posterPath = posterPath,
                        title = title,
                        overview = overview,
                        voteAverage = rating,
                        productionCompanies = productionCompanies.joinToString { it.name },
                        genres = genres.joinToString { it.name },
                        releaseDate = releaseDate
                    )
                }
            }
            is Lce.Error -> {
                showError(R.string.details_downloading_error)
            }
        }
    }

    private fun handleActorsState(newState: Lce<Credits>) {
        state = if (newState is Lce.Content) {
            state.copy(
                credits = newState.value.cast,
                screenState = Lce.Content(Unit)
            )
        } else {
            state.copy(screenState = reduceScreenState(state.screenState, newState))
        }
    }

    private fun reduceScreenState(oldState: Lce<Unit>, newState: Lce<Credits>): Lce<Unit> {
        return when {
            oldState is Lce.Content -> {
                oldState
            }
            newState is Lce.Loading -> {
                Lce.Loading
            }
            else -> {
                Timber.d("Lce.Error")
                Lce.Error(RuntimeException("Error actors downloading"))
            }
        }
    }

    fun onActorClicked(actorId: Int) {
        flipperPoint(Features.Popular.actorDetails) {
            navigateTo(DetailsMovieFragmentDirections.toActorDetails(actorId = actorId))
        }
    }

    fun onTrailersClicked() {
        flipperPoint(Features.Popular.details) {
            navigateTo(DetailsMovieFragmentDirections.toTrailersDialog(movieId = movieId))
        }
    }

    fun onLikeClicked(liked: Boolean) {
        if(liked) saveToDB() else deleteFromDb()
    }

    private fun saveToDB() {
        moviesRepository.saveToFaw(
            MovieDbEntity(
                id = state.id,
                posterPath = state.posterPath,
                adult = state.adult,
                overview = state.overview,
                originalTitle = state.originalTitle,
                originalLanguage = state.originalLanguage,
                title = state.title,
                backdropPath = state.posterPath,
                popularity = state.voteAverage.toDouble(),
                releaseDate = state.releaseDate,
                voteAverage = state.voteAverage.toDouble()
            )
        ).onEach {
            if (it is Lce.Content)
                state = state.copy(isLiked = true)
        }
            .launchIn(viewModelScope)
    }

    private fun deleteFromDb() {
        moviesRepository.delete(state.id)
            .onEach {
                if (it is Lce.Content)
                    state = state.copy(isLiked = false)
            }
            .launchIn(viewModelScope)
    }

    private fun isLiked() {
        moviesRepository.isLiked(state.id)
            .onEach {
                if (it is Lce.Content) {
                    state = state.copy(isLiked = it.value)
                }
            }.launchIn(viewModelScope)
    }

    @AssistedFactory
    interface Factory {
        fun create(
            @Assisted("movieId") movieId: Long
        ): DetailsMovieViewModel
    }
}
