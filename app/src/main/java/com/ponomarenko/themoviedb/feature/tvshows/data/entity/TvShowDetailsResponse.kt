package com.ponomarenko.themoviedb.feature.tvshows.data.entity

import com.ponomarenko.themoviedb.base.data.api.NetworkEntity
import com.ponomarenko.themoviedb.feature.popular.data.entity.ProductionCompanyEntity
import com.ponomarenko.themoviedb.feature.popular.domain.entity.ProductionCompany
import com.ponomarenko.themoviedb.feature.popular.domain.entity.ProductionCountry
import com.ponomarenko.themoviedb.feature.popular.domain.entity.SpokenLanguage
import com.ponomarenko.themoviedb.feature.tvshows.domain.entity.*
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TvShowDetailsResponse(
    val backdrop_path: String?,
    val created_by: List<TvShowOwnerEntity>,
    val episode_run_time: List<Int>,
    val first_air_date: String,
    val genres: List<GenreTvShowEntity>,
    val homepage: String,
    val id: Long,
    val in_production: Boolean,
    val languages: List<String>,
    val last_air_date: String,
    val last_episode_to_air: LastEpisodeEntity,
    val name: String,
    val next_episode_to_air: Any?,
    val networks: List<TvNetworkEntity>,
    val number_of_episodes: Int,
    val number_of_seasons: Int,
    val origin_country: List<String>,
    val original_language: String,
    val original_name: String,
    val overview: String,
    val popularity: Float,
    val poster_path: String?,
    val production_companies: List<ProductionCompanyEntity>,
    val production_countries: List<ProductionCountryEntity>,
    val seasons: List<SeasonEntity>,
    val spoken_languages: List<SpokenLanguageEntity>,
    val status: String,
    val tagline: String,
    val type: String,
    val vote_average: Float,
    val vote_count: Int
) : NetworkEntity<TvShowDetails> {
    override fun toDomain(): TvShowDetails {
        return TvShowDetails(
            backdropPath = backdrop_path,
            created = created_by.map { it.toDomain() },
            episodeRunTime = episode_run_time,
            firstAirDate = first_air_date,
            genres = genres.map { it.toDomain() },
            homepage = homepage,
            id = id,
            inProduction = in_production,
            languages = languages,
            lastAirDate = last_air_date,
            lastEpisodeToAir = last_episode_to_air.toDomain(),
            name = name,
            nextEpisodeToAir = next_episode_to_air,
            networks = networks.map { it.toDomain() },
            numberOfEpisodes = number_of_episodes,
            numberOfSeasons = number_of_seasons,
            originCountry = origin_country,
            originalLanguage = original_language,
            originalName = original_name,
            overview = overview,
            popularity = popularity,
            posterPath = poster_path,
            productionCompanies = production_companies.map { it.toDomain() },
            productionCountries = production_countries.map { it.toDomain() },
            seasons = seasons.map { it.toDomain() },
            spokenLanguages = spoken_languages.map { it.toDomain() },
            status = status,
            tagline = tagline,
            type = type,
            voteAverage = vote_average,
            voteCount = vote_count
        )
    }
}

@JsonClass(generateAdapter = true)
data class TvShowOwnerEntity(
    val id: Int,
    val credit_id: String,
    val name: String,
    val gender: Int,
    val profile_path: String?
) : NetworkEntity<TvShowOwner> {
    override fun toDomain(): TvShowOwner {
        return TvShowOwner(
            id = id,
            creditId = credit_id,
            name = name,
            gender = gender,
            profilePath = profile_path
        )
    }
}

@JsonClass(generateAdapter = true)
data class GenreTvShowEntity(
    val id: Int,
    val name: String
) : NetworkEntity<GenreTvShow> {
    override fun toDomain(): GenreTvShow {
        return GenreTvShow(
            id = id,
            name = name
        )
    }
}

@JsonClass(generateAdapter = true)
data class LastEpisodeEntity(
    val air_date: String?,
    val episode_number: Int,
    val name: String,
    val overview: String,
    val production_code: String,
    val season_number: Int,
    val still_path: String?,
    val vote_average: Float,
    val vote_count: Int
) : NetworkEntity<LastEpisode> {
    override fun toDomain(): LastEpisode {
        return LastEpisode(
            airDate = air_date,
            episodeNumber = episode_number,
            name = name,
            overview = overview,
            productionCode = production_code,
            seasonNumber = season_number,
            stillPath = still_path,
            voteAverage = vote_average,
            voteCount = vote_count
        )
    }
}

@JsonClass(generateAdapter = true)
data class TvNetworkEntity(
    val name: String,
    val id: Int,
    val logo_path: String?,
    val origin_country: String
) : NetworkEntity<TvNetwork> {
    override fun toDomain(): TvNetwork {
        return TvNetwork(
            name = name,
            id = id,
            logoPath = logo_path,
            originCountry = origin_country
        )
    }
}

@JsonClass(generateAdapter = true)
data class SeasonEntity(
    val air_date: String?,
    val episode_count: Int,
    val id: Int,
    val name: String,
    val overview: String,
    val poster_path: String?,
    val season_number: Int
) : NetworkEntity<Season> {
    override fun toDomain(): Season {
        return Season(
            airDate = air_date,
            episodeCount = episode_count,
            id = id,
            name = name,
            overview = overview,
            posterPath = poster_path,
            seasonNumber = season_number,
        )
    }
}

@JsonClass(generateAdapter = true)
data class SpokenLanguageEntity(
    val iso_639_1: String,
    val name: String
) : NetworkEntity<SpokenLanguage> {
    override fun toDomain(): SpokenLanguage {
        return SpokenLanguage(
            iso = iso_639_1,
            name = name
        )
    }
}

@JsonClass(generateAdapter = true)
data class ProductionCountryEntity(
    val iso_3166_1: String,
    val name: String
) : NetworkEntity<ProductionCountry> {
    override fun toDomain(): ProductionCountry {
        return ProductionCountry(
            iso = iso_3166_1,
            name = name
        )
    }
}

@JsonClass(generateAdapter = true)
data class ProductionCompanyEntity(
    val id: Int,
    val logo_path: String?,
    val name: String,
    val origin_country: String
) : NetworkEntity<ProductionCompany> {
    override fun toDomain(): ProductionCompany {
        return ProductionCompany(
            id = id,
            logoPath = logo_path,
            name = name,
            originCountry = origin_country
        )
    }
}
