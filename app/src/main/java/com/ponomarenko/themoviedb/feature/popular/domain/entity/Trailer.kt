package com.ponomarenko.themoviedb.feature.popular.domain.entity

import com.ponomarenko.themoviedb.BuildConfig

data class Trailer(
    val id: String,
    val key: String,
    val name: String,
    val type: VideoType
) {
    val imagePath: String
    get() = "${BuildConfig.TRAILER_IMAGE_BASE_URL}$id"
}

enum class VideoType {
    TRAILER, TEASER, CLIP, FEATURETTE, BEHIND_SCENES, BLOOPERS
}
