package com.ponomarenko.themoviedb.feature.popular.presentation.list

import androidx.paging.PagingData
import com.ponomarenko.themoviedb.feature.base.presentation.MoviesViewState
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Movie

data class PopularMoviesViewState(
    val isRefreshing: Boolean,
    override val item: PagingData<Movie>,
) : MoviesViewState<Movie>
