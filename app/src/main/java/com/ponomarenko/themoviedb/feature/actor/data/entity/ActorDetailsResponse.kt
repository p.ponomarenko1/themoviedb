package com.ponomarenko.themoviedb.feature.actor.data.entity

import com.ponomarenko.themoviedb.base.data.api.NetworkEntity
import com.ponomarenko.themoviedb.feature.actor.domain.entity.ActorDetails
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ActorDetailsResponse(
    val birthday: String?,
    val known_for_department: String,
    val deathday: String?,
    val id: Int,
    val name: String,
    val also_known_as: List<String>,
    val gender: Int,
    val biography: String,
    val popularity: Float,
    val place_of_birth: String?,
    val profile_path: String?,
    val adult: Boolean,
    val imdb_id: String,
    val homepage: String?
) : NetworkEntity<ActorDetails> {
    override fun toDomain(): ActorDetails {
        return ActorDetails(
            birthday = birthday,
            department = known_for_department,
            deathday = deathday,
            id = id,
            name = name,
            knownAs = also_known_as,
            biography = biography,
            popularity = popularity,
            placeOfBirth = place_of_birth,
            profilePath = profile_path,
        )
    }
}
