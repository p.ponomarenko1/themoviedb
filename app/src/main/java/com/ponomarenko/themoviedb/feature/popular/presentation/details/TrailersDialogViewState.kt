package com.ponomarenko.themoviedb.feature.popular.presentation.details

import com.ponomarenko.themoviedb.base.domain.lce.Lce
import com.ponomarenko.themoviedb.base.presentation.viewmodel.ViewState
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Trailer

data class TrailersDialogViewState(
    val screenState: Lce<Unit> = Lce.Loading,
    val trailers: List<Trailer> = emptyList()
) : ViewState
