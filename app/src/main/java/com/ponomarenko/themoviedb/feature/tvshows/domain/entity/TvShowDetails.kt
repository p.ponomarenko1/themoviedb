package com.ponomarenko.themoviedb.feature.tvshows.domain.entity

import com.ponomarenko.themoviedb.feature.popular.domain.entity.ProductionCompany
import com.ponomarenko.themoviedb.feature.popular.domain.entity.ProductionCountry
import com.ponomarenko.themoviedb.feature.popular.domain.entity.SpokenLanguage

data class TvShowDetails(
    val backdropPath: String?,
    val created: List<TvShowOwner>,
    val episodeRunTime: List<Int>,
    val firstAirDate: String,
    val genres: List<GenreTvShow>,
    val homepage: String,
    val id: Long,
    val inProduction: Boolean,
    val languages: List<String>,
    val lastAirDate: String,
    val lastEpisodeToAir: LastEpisode,
    val name: String,
    val nextEpisodeToAir: Any?,
    val networks: List<TvNetwork>,
    val numberOfEpisodes: Int,
    val numberOfSeasons: Int,
    val originCountry: List<String>,
    val originalLanguage: String,
    val originalName: String,
    val overview: String,
    val popularity: Float,
    val posterPath: String?,
    val productionCompanies: List<ProductionCompany>,
    val productionCountries: List<ProductionCountry>,
    val seasons: List<Season>,
    val spokenLanguages: List<SpokenLanguage>,
    val status: String,
    val tagline: String,
    val type: String,
    val voteAverage: Float,
    val voteCount: Int
) {
    val rating: Float
    get() = voteAverage.div(2)
}
