package com.ponomarenko.themoviedb.feature.popular.data

import com.ponomarenko.themoviedb.base.domain.lce.Lce
import com.ponomarenko.themoviedb.base.domain.lce.mapToLce
import com.ponomarenko.themoviedb.feature.base.data.TrailersRepository
import com.ponomarenko.themoviedb.feature.popular.data.api.PopularMoviesApi
import com.ponomarenko.themoviedb.feature.popular.domain.entity.TrailersResult
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class PopularMovieTrailersRepository @Inject constructor(
    private val api: PopularMoviesApi
) : TrailersRepository<TrailersResult> {
    override fun getTrailersStream(movieId: Long): Flow<Lce<TrailersResult>> =
        flow {
            val trailers = api.getTrailersList(movieId = movieId).toDomain()
            emit(trailers)
        }.mapToLce()
}
