package com.ponomarenko.themoviedb.feature.popular.data

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.ponomarenko.themoviedb.feature.base.data.MoviesRepository
import com.ponomarenko.themoviedb.base.domain.lce.Lce
import com.ponomarenko.themoviedb.base.domain.lce.mapToLce
import com.ponomarenko.themoviedb.feature.base.data.MoviesDao
import com.ponomarenko.themoviedb.feature.popular.data.api.PopularMoviesApi
import com.ponomarenko.themoviedb.feature.popular.domain.entity.MovieDetails
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Movie
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class PopularMoviesRepository @Inject constructor(
    private val api: PopularMoviesApi,
) : MoviesRepository<Movie> {

    override fun getMoviesStream(page: Int): Flow<PagingData<Movie>> =
        Pager(
            config = PagingConfig(
                pageSize = page,
                initialLoadSize = page + 1,
                enablePlaceholders = false,
            ),
            pagingSourceFactory = { PopularMoviesSource(api) }
        ).flow
}
