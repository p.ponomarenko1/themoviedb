package com.ponomarenko.themoviedb.feature.base.data

import com.ponomarenko.themoviedb.base.domain.lce.Lce
import com.ponomarenko.themoviedb.feature.actor.domain.entity.ActorDetails
import com.ponomarenko.themoviedb.feature.actor.domain.entity.ActorCredits
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Credits
import kotlinx.coroutines.flow.Flow

interface ActorRepository {
    fun getActorsStream(movieId: Long): Flow<Lce<Credits>>
    fun getDetailsStream(id: Int): Flow<Lce<ActorDetails>>
    fun getPersonCreditsStream(personId: Int): Flow<Lce<ActorCredits>>
}
