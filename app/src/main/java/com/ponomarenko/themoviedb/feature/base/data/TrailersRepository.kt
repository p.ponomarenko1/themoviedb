package com.ponomarenko.themoviedb.feature.base.data

import com.ponomarenko.themoviedb.base.domain.lce.Lce
import kotlinx.coroutines.flow.Flow

interface TrailersRepository<T : Any> {
    fun getTrailersStream(movieId: Long): Flow<Lce<T>>
}
