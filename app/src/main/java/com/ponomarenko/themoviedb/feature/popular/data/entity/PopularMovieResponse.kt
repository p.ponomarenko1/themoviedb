package com.ponomarenko.themoviedb.feature.popular.data.entity

import com.ponomarenko.themoviedb.base.data.api.NetworkEntity
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Movie
import com.ponomarenko.themoviedb.feature.popular.domain.entity.PopularMovieResult
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PopularMovieResultResponse(
    val page: Int,
    val results: List<PopularMovieResponseEntity>,
) : NetworkEntity<PopularMovieResult> {

    override fun toDomain(): PopularMovieResult {
        return PopularMovieResult(
            page = page,
            movies = results.map { it.toDomain() },
        )
    }
}


@JsonClass(generateAdapter = true)
data class PopularMovieResponseEntity(
    val poster_path: String?,
    val adult: Boolean,
    val overview: String,
    val genre_ids: List<Int>,
    val id: Long,
    val original_title: String,
    val original_language: String,
    val title: String,
    val backdrop_path: String?,
    val popularity: Double,
    val vote_count: Int,
    val release_date: String?,
    val video: Boolean,
    val vote_average: Double
) : NetworkEntity<Movie> {

    override fun toDomain(): Movie {
        return Movie(
            posterPath = poster_path,
            adult = adult,
            overview = overview,
            genreIds = genre_ids,
            id = id,
            originalTitle = original_title,
            originalLanguage = original_language,
            title = title,
            backdropPath = backdrop_path,
            popularity = popularity,
            releaseDate = release_date,
            voteAverage = vote_average
        )
    }
}
