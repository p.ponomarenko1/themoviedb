package com.ponomarenko.themoviedb.feature.popular.data.entity

import com.ponomarenko.themoviedb.base.data.api.NetworkEntity
import com.ponomarenko.themoviedb.feature.popular.domain.entity.Trailer
import com.ponomarenko.themoviedb.feature.popular.domain.entity.TrailersResult
import com.ponomarenko.themoviedb.feature.popular.domain.entity.VideoType
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TrailersResultResponse(
    val id: Long,
    val results: List<TrailerNetworkEntity>
) : NetworkEntity<TrailersResult> {
    override fun toDomain(): TrailersResult {
        return TrailersResult(
            id = id,
            results = results.map { it.toDomain() }
        )
    }
}

@JsonClass(generateAdapter = true)
data class TrailerNetworkEntity(
    val id: String,
    val iso_639_1: String,
    val iso_3166_1: String,
    val key: String,
    val name: String,
    val site: String,
    val size: Int,
    val type: VideoTypeNetworkEntity
) : NetworkEntity<Trailer> {
    override fun toDomain(): Trailer {
        return Trailer(
            id = id,
            key = key,
            name = name,
            type = when (type) {
                VideoTypeNetworkEntity.TRAILER -> VideoType.TRAILER
                VideoTypeNetworkEntity.TEASER -> VideoType.TEASER
                VideoTypeNetworkEntity.CLIP -> VideoType.CLIP
                VideoTypeNetworkEntity.FEATURETTE -> VideoType.FEATURETTE
                VideoTypeNetworkEntity.BEHIND_SCENES -> VideoType.BEHIND_SCENES
                VideoTypeNetworkEntity.BLOOPERS -> VideoType.BLOOPERS
            }
        )
    }
}


@JsonClass(generateAdapter = false)
enum class VideoTypeNetworkEntity {
    @Json(name = "Trailer")
    TRAILER,

    @Json(name = "Teaser")
    TEASER,

    @Json(name = "Clip")
    CLIP,

    @Json(name = "Featurette")
    FEATURETTE,

    @Json(name = "Behind the Scenes")
    BEHIND_SCENES,

    @Json(name = "Bloopers")
    BLOOPERS
}
