package com.ponomarenko.themoviedb.feature.tvshows.data.entity

import com.ponomarenko.themoviedb.base.data.api.NetworkEntity
import com.ponomarenko.themoviedb.feature.tvshows.domain.entity.TvShow
import com.ponomarenko.themoviedb.feature.tvshows.domain.entity.TvShowsDomainEntity
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TvShowsResponse(
    val page: Int,
    val results: List<TvShowNetworkEntity>,
    val total_results: Int,
    val total_pages: Int
) : NetworkEntity<TvShowsDomainEntity> {
    override fun toDomain(): TvShowsDomainEntity {
        return TvShowsDomainEntity(
            page = page,
            result = results.map { it.toDomain() },
            totalResults = total_results,
            totalPages = total_pages
        )
    }
}

@JsonClass(generateAdapter = true)
data class TvShowNetworkEntity(
    val poster_path: String?,
    val popularity: Float,
    val id: Long,
    val backdrop_path: String?,
    val vote_average: Float,
    val overview: String,
    val first_air_date: String?,
    val origin_country: List<String>,
    val genre_ids: List<Int>,
    val original_language: String,
    val vote_count: Int,
    val name: String,
    val original_name: String
): NetworkEntity<TvShow> {
    override fun toDomain(): TvShow {
        return TvShow(
            posterPath = poster_path,
            popularity = popularity,
            id = id,
            backdropPath = backdrop_path,
            voteAverage = vote_average,
            overview = overview,
            firstAirDate = first_air_date,
            originCountry = origin_country,
            genreIds = genre_ids,
            originalLanguage = original_language,
            voteCount = vote_count,
            name = name,
            originalName = original_name
        )
    }
}
