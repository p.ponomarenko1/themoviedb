package com.ponomarenko.themoviedb.feature.popular.presentation.details

import androidx.lifecycle.viewModelScope
import com.ponomarenko.themoviedb.base.domain.lce.Lce
import com.ponomarenko.themoviedb.base.presentation.viewmodel.StatefulViewModel
import com.ponomarenko.themoviedb.feature.popular.data.PopularMovieTrailersRepository
import com.ponomarenko.themoviedb.feature.popular.domain.entity.TrailersResult
import com.redmadrobot.extensions.lifecycle.mapDistinct
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class TrailersDialogViewModel @AssistedInject constructor(
    @Assisted("movieId") private val movieId: Long,
    private val trailersRepository: PopularMovieTrailersRepository
) : StatefulViewModel<TrailersDialogViewState>() {

    val screenStateWithItems = liveState.mapDistinct { it.screenState to it.trailers }

    init {
        state = TrailersDialogViewState(
            screenState = Lce.Loading,
            trailers = emptyList()
        )
        fetchTrailersList()
    }

    private fun fetchTrailersList() {
        trailersRepository.getTrailersStream(movieId = movieId)
            .onEach { newState ->
                state = if (newState is Lce.Content) state.copy(
                    trailers = newState.value.results,
                    screenState = Lce.Content(Unit)
                )
                else
                    state.copy(screenState = reduceScreenState(state.screenState, newState))

            }.launchIn(viewModelScope)
    }

    private fun reduceScreenState(oldState: Lce<Unit>, newState: Lce<TrailersResult>): Lce<Unit> {
        return when {
            oldState is Lce.Content -> oldState
            newState is Lce.Loading -> Lce.Loading
            else -> Lce.Error(RuntimeException("Error trailers downloading"))
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(@Assisted("movieId") movieId: Long): TrailersDialogViewModel
    }
}



