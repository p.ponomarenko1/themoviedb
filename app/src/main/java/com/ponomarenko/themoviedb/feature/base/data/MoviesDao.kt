package com.ponomarenko.themoviedb.feature.base.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.ponomarenko.themoviedb.feature.base.data.entity.MovieDbEntity

@Dao
interface MoviesDao {

    @Insert(onConflict = REPLACE)
    suspend fun insert(movie: MovieDbEntity)

    @Query("DELETE FROM movies WHERE id = :id")
    suspend fun delete(id: Long)

    @Query("SELECT * FROM movies WHERE id = :id LIMIT 1")
    suspend fun getById(id: Long): MovieDbEntity?

    @Query("SELECT * FROM movies")
    suspend fun getAll(): List<MovieDbEntity>
}
