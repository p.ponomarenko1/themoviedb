package com.ponomarenko.themoviedb.base.presentation.ui.view

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout

/**
 * Центрирует [centerViews] относительно своей видимой части.
 * При изменениях, не касающихся лэйаута, [center] нужно вызывать извне,
 */
abstract class VisibleCenterContainer @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : FrameLayout(context, attrs, defStyleAttr) {

    protected abstract val centerViews: List<View>

    private val visibleRect = Rect()

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        center()
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        center()
    }

    fun center(): Boolean {
        getGlobalVisibleRect(visibleRect)
        centerViews.forEach { centerView ->
            centerView.y = (visibleRect.height() - centerView.height) / 2f
        }
        return centerViews.isNotEmpty()
    }
}
