package com.ponomarenko.themoviedb.base.presentation.ui.dialog

import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.core.view.children
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.ponomarenko.themoviedb.R
import com.ponomarenko.themoviedb.base.presentation.entity.getString
import com.ponomarenko.themoviedb.base.presentation.extensions.navigateSafe
import com.ponomarenko.themoviedb.base.presentation.ui.BaseActivity
import com.ponomarenko.themoviedb.base.presentation.ui.SnackbarHandler
import com.ponomarenko.themoviedb.base.presentation.viewmodel.ErrorMessageEvent
import com.ponomarenko.themoviedb.base.presentation.viewmodel.MessageEvent
import com.ponomarenko.themoviedb.base.presentation.viewmodel.NavigationEvent
import com.ponomarenko.ui.bottomsheet.MovieBottomSheetDialogFragment
import com.ponomarenko.ui.snackbar.SnackbarStyle
import com.ponomarenko.ui.snackbar.setStyle
import com.redmadrobot.extensions.lifecycle.Event


abstract class BaseBottomSheetDialogFragment(
    @LayoutRes layoutId: Int
) : MovieBottomSheetDialogFragment(layoutId) {

    private val activitySnackbarHandler = ActivitySnackbarHandler()
    private val dialogSnackbarHandler = DialogSnackbarHandler()

    @get:IdRes
    protected open val activitySnackbarAnchorView: Int? = null

    @get:IdRes
    protected open val dialogSnackbarAnchorView: Int? = null

    /** Функция для обработки событий, которые прилетают из ViewModel. */
    @CallSuper
    protected open fun onEvent(event: Event) {
        when (event) {
            is MessageEvent -> {
                val handler = if (event.dispatchToActivity) activitySnackbarHandler else dialogSnackbarHandler
                handler.showSnackbar(
                    getString(event.message),
                style = if (event.urgent) SnackbarStyle.UrgentMessage else SnackbarStyle.Message
                )
            }
            is ErrorMessageEvent -> {
                val handler = if (event.dispatchToActivity) activitySnackbarHandler else dialogSnackbarHandler
                handler.showSnackbar(getString(event.message), style = SnackbarStyle.Error)
            }
            is NavigationEvent -> handleNavigationEvent(event)
        }
    }

    protected open fun handleNavigationEvent(event: NavigationEvent) {
        val controller = if (event.rootGraph) getRootNavController() else getNavController()
        when (event) {
            is NavigationEvent.ToDirection -> controller.navigateSafe(event.direction, extras = event.extras)
            is NavigationEvent.PopBackStack -> controller.popBackStack(event.destinationId, event.inclusive)
            is NavigationEvent.Up -> if(!controller.navigateUp()) getRootNavController().navigateUp()
        }
        dismiss()
    }

    protected open fun getNavController(): NavController = findNavController()

    protected fun getRootNavController(): NavController = requireActivity().findNavController(R.id.main_container)

    private inner class ActivitySnackbarHandler : SnackbarHandler {
        override val snackbarAnchorView: Int? get() = activitySnackbarAnchorView
        override fun showSnackbar(
            text: String,
            duration: Int,
            actionTitleId: Int?,
            action: ((View) -> Unit)?,
            containerReId: Int,
            anchorViewId: Int?,
            style: SnackbarStyle
        ) {
            (activity as? BaseActivity)?.showSnackbar(
                text = text,
                duration = duration,
                actionTitleId = actionTitleId,
                action = action,
                containerResId = containerReId,
                anchorViewId = anchorViewId,
                style = style,
            )
        }
    }

    private inner class DialogSnackbarHandler : SnackbarHandler {
        override val snackbarAnchorView: Int? get() = dialogSnackbarAnchorView
        override fun showSnackbar(
            text: String,
            duration: Int,
            actionTitleId: Int?,
            action: ((View) -> Unit)?,
            containerReId: Int,
            anchorViewId: Int?,
            style: SnackbarStyle
        ) {
            val viewGroup = dialog?.findViewById<ViewGroup>(containerReId) ?: return
            Snackbar.make(viewGroup, text, duration)
                .apply {
                    view.elevation = (viewGroup.children.maxOfOrNull { it.elevation } ?: 0f) + 1
                    actionTitleId?.let { setAction(it, action) }
                    anchorViewId?.let(::setAnchorView)
                    setStyle(style)
                }
                .show()
        }

    }
}
