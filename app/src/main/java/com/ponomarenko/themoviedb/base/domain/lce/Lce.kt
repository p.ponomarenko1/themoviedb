package com.ponomarenko.themoviedb.base.domain.lce

sealed class Lce<out T> {
    companion object {
        fun <T> of(content: T): Lce<T> = Content(content)
    }

    object Loading : Lce<Nothing>() {
        override fun toString() = "Loading"
    }

    data class Content<V>(val value: V) : Lce<V>()
    data class Error(val throwable: Throwable) : Lce<Nothing>()
}

fun Lce<*>.isContent() = this is Lce.Content

fun Lce<*>.isLoading() = this is Lce.Loading

fun Lce<*>.isError() = this is Lce.Error

fun <T> Lce<T>.getContentOrNull(): T? = (this as? Lce.Content)?.value

fun Lce<*>.getErrorOrNull(): Throwable? = (this as? Lce.Error)?.throwable

inline fun <T, R> Lce<T>.fold(
    onLoading: () -> R,
    onError: (Throwable) -> R,
    onContent: (T) -> R
): R {
    return when (this) {
        is Lce.Loading -> onLoading()
        is Lce.Error -> onError(throwable)
        is Lce.Content -> onContent(value)
    }
}
