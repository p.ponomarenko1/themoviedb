package com.ponomarenko.themoviedb.base.presentation.entity

import androidx.annotation.StringRes

data class ButtonAction(
    @StringRes val titleRes: Int,
    val run: () -> Unit,
)
