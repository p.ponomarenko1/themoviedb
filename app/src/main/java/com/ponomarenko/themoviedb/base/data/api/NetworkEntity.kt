package com.ponomarenko.themoviedb.base.data.api

interface NetworkEntity<out T> {
    fun toDomain(): T
}
