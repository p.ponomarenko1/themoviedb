package com.ponomarenko.themoviedb.base.presentation.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import androidx.annotation.DrawableRes
import androidx.annotation.LayoutRes
import androidx.appcompat.widget.AppCompatImageView
import com.airbnb.epoxy.EpoxyController
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import com.ponomarenko.themoviedb.R

@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT)
class ShimmerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : FrameLayout(context, attrs, defStyleAttr) {

    companion object {
        private const val CONTENT_GROUP = "content"
    }

    @ModelProp(group = CONTENT_GROUP)
    fun setShimmerLayout(@LayoutRes layout: Int) {
        removeAllViewsInLayout()
        inflate(context, layout, this)
    }

    @ModelProp(group = CONTENT_GROUP)
    fun setShimmerImage(@DrawableRes imageRes: Int) {
        removeAllViewsInLayout()
        val imageView = AppCompatImageView(context).apply {
            setImageResource(imageRes)
        }
        addView(imageView)
    }

    @ModelProp
    fun setWrapWidth(wrapWidth: Boolean) {
        val width = if (wrapWidth) LayoutParams.WRAP_CONTENT else LayoutParams.MATCH_PARENT
        layoutParams = LayoutParams(width, LayoutParams.WRAP_CONTENT)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        startAnimation(AnimationUtils.loadAnimation(context, R.anim.movie_blink))
    }
}

fun EpoxyController.imageShimmers(@DrawableRes imageRes: Int, count: Int = 1) {
    for (i in 1..count) {
        shimmerView {
            id(imageRes, i)
            shimmerImage(imageRes)
        }
    }
}

fun EpoxyController.layoutShimmers(
    @LayoutRes layout: Int,
    isNeedWapWidth: Boolean = false,
    count: Int = 1
) {
    for (i in 1..count) {
        shimmerView {
            id(layout, i)
            shimmerLayout(layout)
            wrapWidth(isNeedWapWidth)
        }
    }
}
