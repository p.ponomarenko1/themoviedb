package com.ponomarenko.themoviedb.base.data

import android.content.Context
import com.ponomarenko.themoviedb.base.domain.ResourceReader

class AndroidResourceReader(private val context: Context) : ResourceReader {
    override fun getString(res: Int, vararg args: Any): String = context.getString(res, *args)
}
