package com.ponomarenko.themoviedb.base.presentation.ui.view

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.annotation.StringRes
import androidx.core.view.updatePadding
import com.airbnb.epoxy.CallbackProp
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import com.airbnb.epoxy.TextProp
import com.airbnb.epoxy.paging3.PagingDataEpoxyController
import com.ponomarenko.themoviedb.R
import com.ponomarenko.themoviedb.base.presentation.entity.ButtonAction
import com.ponomarenko.themoviedb.base.presentation.entity.Paddings
import com.ponomarenko.themoviedb.base.presentation.entity.updatePadding
import com.ponomarenko.themoviedb.databinding.CellPaginationErrorBinding
import com.redmadrobot.extensions.resources.dpToPx
import com.redmadrobot.extensions.viewbinding.inflateViewBinding
import kotlinx.coroutines.ObsoleteCoroutinesApi

@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT)
class PaginationErrorCell @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : LinearLayout(context, attrs, defStyleAttr), SpacingBehavior {

    companion object {
        private const val PADDING_VERTICAL_DP = 40
    }

    private val binding: CellPaginationErrorBinding = inflateViewBinding()

    override val handlesVerticalSpacing = true

    init {
        orientation = VERTICAL
        val paddingVertical = context.dpToPx(PADDING_VERTICAL_DP)
        updatePadding(top = paddingVertical, bottom = paddingVertical)
    }

    @TextProp
    fun setText(text: CharSequence) {
        binding.paginationErrorText.text = text
    }

    @ModelProp
    fun setPaddings(paddings: Paddings?) {
        paddings?.also(::updatePadding)
    }

    @CallbackProp
    fun setAction(action: ButtonAction?) {
        with(binding.paginationErrorRetry) {
            action?.apply {
                setText(titleRes)
                setOnClickListener { run() }
            } ?: setOnClickListener(null)
        }
    }
}

@ObsoleteCoroutinesApi
fun PagingDataEpoxyController<*>.paginationErrorCell(
    @StringRes text: Int,
    paddings: Paddings? = null,
    @StringRes buttonText: Int = R.string.try_again,
) {
    paginationErrorCell {
        id("pagination_error")
        text(text)
        paddings(paddings)
        action(ButtonAction(buttonText) { retry() })
    }
}
