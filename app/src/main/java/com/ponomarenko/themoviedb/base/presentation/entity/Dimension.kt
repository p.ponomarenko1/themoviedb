package com.ponomarenko.themoviedb.base.presentation.entity

import android.view.View
import androidx.annotation.DimenRes
import com.redmadrobot.extensions.resources.dpToPx
import com.redmadrobot.extensions.resources.getDimensionPixelOffset

sealed class Dimension {
    data class Res(@DimenRes val res: Int) : Dimension()
    data class Dp(val dp: Int) : Dimension()
    data class Px(@androidx.annotation.Px val px: Int) : Dimension()
}

fun Int.dp() = Dimension.Dp(this)

fun Int.px() = Dimension.Px(this)

fun View.getDimension(dimension: Dimension): Int {
    return when (dimension) {
        is Dimension.Res -> getDimensionPixelOffset(dimension.res)
        is Dimension.Dp -> context.dpToPx(dimension.dp)
        is Dimension.Px -> dimension.px
    }
}
