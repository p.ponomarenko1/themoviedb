package com.ponomarenko.themoviedb.base.domain

import androidx.annotation.StringRes

interface ResourceReader {
    fun getString(@StringRes res: Int, vararg args: Any): String
}
