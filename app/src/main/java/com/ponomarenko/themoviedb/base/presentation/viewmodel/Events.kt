package com.ponomarenko.themoviedb.base.presentation.viewmodel

import androidx.navigation.NavDirections
import androidx.navigation.Navigator
import com.ponomarenko.themoviedb.base.presentation.entity.Text
import com.redmadrobot.extensions.lifecycle.Event

data class MessageEvent(val message: Text, val urgent: Boolean, val dispatchToActivity: Boolean) : Event
data class ErrorMessageEvent(val message: Text, val dispatchToActivity: Boolean) : Event

sealed class NavigationEvent : Event {

    abstract val rootGraph: Boolean

    data class Up(override val rootGraph: Boolean): NavigationEvent()

    data class ToDirection(
        val direction: NavDirections,
        val extras: Navigator.Extras? = null,
        override val rootGraph: Boolean
    ) : NavigationEvent()

    data class PopBackStack(
        val destinationId: Int,
        val inclusive: Boolean,
        override val rootGraph: Boolean
    ) : NavigationEvent()
}
