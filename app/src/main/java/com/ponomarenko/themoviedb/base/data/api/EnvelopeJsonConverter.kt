package com.ponomarenko.themoviedb.base.data.api

import com.squareup.moshi.JsonClass
import com.squareup.moshi.Types
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type

/**
 * Конвертер серверных ответов, построенных на принципе помещения полезной нагрузки в Json-
 * объект data.
 * Результат конвертации - содержимое data.
 * Позволяет описывать возвращаемое значение запроса в Api в целевом виде - без обертки в data.
 *
 * Следует применять, когда все серверные ответы или подавляющее их большинство построено
 * вышеописанным образом.
 */
class EnvelopeJsonConverter<T>(
    private val delegate: Converter<ResponseBody, Envelope<T>>,
) : Converter<ResponseBody, T> {

    class Factory : Converter.Factory() {
        override fun responseBodyConverter(
            type: Type,
            annotations: Array<Annotation>,
            retrofit: Retrofit,
        ): Converter<ResponseBody, *>? {
            val envelopeType = Types.newParameterizedTypeWithOwner(
                EnvelopeJsonConverter::class.java,
                Envelope::class.java,
                type
            )
            val delegate = retrofit.nextResponseBodyConverter<Envelope<Any?>>(this, envelopeType, annotations)
            return EnvelopeJsonConverter(delegate)
        }
    }

    override fun convert(responseBody: ResponseBody): T? {
        val envelope = delegate.convert(responseBody)
        return envelope?.data
    }

    @JsonClass(generateAdapter = true)
    class Envelope<T>(internal val data: T)
}
