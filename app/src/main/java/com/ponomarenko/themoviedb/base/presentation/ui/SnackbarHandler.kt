package com.ponomarenko.themoviedb.base.presentation.ui

import android.view.View
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar
import com.ponomarenko.themoviedb.R
import com.ponomarenko.ui.snackbar.SnackbarStyle
import java.time.Duration

interface SnackbarHandler {

    /**
     * Привязка [Snackbar] к конкретному
     * [CoordinatorLayout][androidx.coordinatorlayout.widget.CoordinatorLayout].
     *
     * По умолчанию [Snackbar] отрисовывается на уровне activity в [android.R.id.content].
     * В случаях где [Snackbar] должен отрисовывается в другом контейнере, например, во вложенном
     * фрагменте из раздела, нужно переопределить это поле.
     *
     * @see Snackbar.findSuitableParent
     */

    @get:IdRes
    val snackbarContainer: Int
    get() = R.id.coordinator

    /**
     * Привязка [Snackbar] к конкретному элементу экрана.
     * Например, чтобы показать его над кнопками или другими элементами.
     *
     * @see snackbarContainer
     */

    @get:IdRes
    val snackbarAnchorView: Int?
        get() = null

    fun showSnackbar(
        text: String,
        duration: Int = Snackbar.LENGTH_LONG,
        @StringRes actionTitleId: Int? = null,
        action: ((View) -> Unit)? = null,
        @IdRes containerReId: Int = snackbarContainer,
        @IdRes anchorViewId: Int? = snackbarAnchorView,
        style: SnackbarStyle = SnackbarStyle.Message,
    )
}
