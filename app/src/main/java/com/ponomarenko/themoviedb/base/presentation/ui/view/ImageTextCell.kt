package com.ponomarenko.themoviedb.base.presentation.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.airbnb.epoxy.CallbackProp
import com.airbnb.epoxy.ModelView
import com.airbnb.epoxy.TextProp
import com.ponomarenko.themoviedb.R
import com.ponomarenko.themoviedb.base.presentation.extensions.loadImage
import com.ponomarenko.themoviedb.databinding.ViewCellImageTextBinding
import com.ponomarenko.ui.extensions.resolveResourceId
import com.redmadrobot.extensions.resources.getDimension
import com.redmadrobot.extensions.viewbinding.inflateViewBinding

@ModelView(autoLayout = ModelView.Size.WRAP_WIDTH_WRAP_HEIGHT)
class ImageTextCell @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : LinearLayout(context, attrs, defStyleAttr), SpacingBehavior {

    private val binding: ViewCellImageTextBinding = inflateViewBinding()

    override val handlesHorizontalSpacing = true
    override val handlesVerticalSpacing = true

    init {
        setBackgroundResource(resolveResourceId(android.R.attr.selectableItemBackground))

        val paddingHorizontal = getDimension(R.dimen.cell_horizontal_padding).toInt()
        val paddingVertical = getDimension(R.dimen.cell_vertical_padding).toInt()
        setPadding(paddingHorizontal, paddingVertical, paddingHorizontal, paddingVertical)

        clipToPadding = false
        orientation = VERTICAL
    }

    @TextProp
    fun setImage(url: CharSequence?) {
        loadPoster(url)
    }

    @TextProp
    fun setTitle(title: CharSequence) {
        binding.cellTitle.text = title
    }

    @CallbackProp
    fun setOnClickListener(listener: ((View) -> Unit)?) {
        super.setOnClickListener(listener)
    }

    private fun loadPoster(url: CharSequence?) {
        binding.cellImage.loadImage(url)
    }
}
