@file:Suppress("NOTHING_TO_INLINE")

package com.ponomarenko.themoviedb.base.domain.lce

@Suppress("UNCHECKED_CAST")
inline fun <T, R> Lce<T>.mapContent(transform: (T) -> R): Lce<R> {
    return when (this) {
        Lce.Loading, is Lce.Error -> this as Lce<R>
        is Lce.Content -> Lce.Content(transform(value))
    }
}

@Suppress("UNCHECKED_CAST")
inline fun <T, R> Lce<List<T>>.mapContentList(transform: (T) -> R): Lce<List<R>> {
    return when (this) {
        Lce.Loading, is Lce.Error -> this as Lce<List<R>>
        is Lce.Content -> Lce.Content(value.map(transform))
    }
}

@Suppress("UNCHECKED_CAST")
inline fun Lce<*>.ignoreContent(): Lce<Unit> {
    return when (this) {
        Lce.Loading, is Lce.Error -> this as Lce<Unit>
        is Lce.Content -> Lce.Content(Unit)
    }
}

inline operator fun <T> Lce<T>.component1(): Lce<Unit> = ignoreContent()
inline operator fun <T> Lce<T>.component2(): T? = getContentOrNull()
