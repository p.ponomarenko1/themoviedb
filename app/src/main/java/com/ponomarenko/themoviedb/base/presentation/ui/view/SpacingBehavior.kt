package com.ponomarenko.themoviedb.base.presentation.ui.view

/**
 * Интерфейс, определяющий поведение вьюхи когда к ней применяется Decoration.
 * Позволяет вьюхе добавить отступы самостоятельно, игнорируя декоратор.
 */
interface SpacingBehavior {
    val handlesHorizontalSpacing: Boolean get() = false
    val handlesVerticalSpacing: Boolean get() = false
}
