package com.ponomarenko.themoviedb.base.data.coil

import coil.bitmap.BitmapPool
import coil.decode.DataSource
import coil.decode.Options
import coil.fetch.FetchResult
import coil.fetch.Fetcher
import coil.fetch.SourceResult
import coil.size.Size
import com.ponomarenko.themoviedb.base.domain.EncodedImage
import okio.buffer
import okio.source

class EncodedImageFetcher : Fetcher<EncodedImage> {

    override suspend fun fetch(
        pool: BitmapPool,
        data: EncodedImage,
        size: Size,
        options: Options
    ): FetchResult {
        return SourceResult(
            source = data.decode().inputStream().source().buffer(),
            mimeType = null,
            dataSource = DataSource.MEMORY
        )
    }

    override fun key(data: EncodedImage): String? = null
}
