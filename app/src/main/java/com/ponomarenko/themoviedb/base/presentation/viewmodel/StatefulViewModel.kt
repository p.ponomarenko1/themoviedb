package com.ponomarenko.themoviedb.base.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.redmadrobot.extensions.lifecycle.EventQueue
import com.redmadrobot.extensions.lifecycle.provideDelegate

abstract class StatefulViewModel<T : ViewState>(initialState: T? = null) : ViewModel(), EventsDispatcher {

    protected val liveState = MutableLiveData<T>().apply {
        initialState?.let(::setValue)
    }

    protected var state: T by liveState

    override val events: EventQueue = EventQueue()
}
