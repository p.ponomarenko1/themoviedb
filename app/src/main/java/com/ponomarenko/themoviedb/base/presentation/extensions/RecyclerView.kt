package com.ponomarenko.themoviedb.base.presentation.extensions

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/** Возвращает адаптер или бросает исключение, если его нет. */
fun RecyclerView.requireAdapter(): RecyclerView.Adapter<RecyclerView.ViewHolder> =
    checkNotNull(adapter)

/** Устанавливает [LinearLayoutManager] в [RecyclerView] с указанной ориентацией. */
fun RecyclerView.setupLinearLayoutManager(
    @RecyclerView.Orientation orientation: Int = RecyclerView.VERTICAL,
    reverseLayout: Boolean = false,
) {
    layoutManager = LinearLayoutManager(context, orientation, reverseLayout)
}

/**
 * Возвращает LayoutManager заданного типа [T] или бросает исключение, если его нет или тип не
 * совпадает.
 */
inline fun <reified T : RecyclerView.LayoutManager> RecyclerView.requireLayoutManager(): T {
    return checkNotNull(layoutManager) as T
}
