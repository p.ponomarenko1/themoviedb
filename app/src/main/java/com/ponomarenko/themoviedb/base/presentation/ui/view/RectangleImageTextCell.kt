package com.ponomarenko.themoviedb.base.presentation.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import com.airbnb.epoxy.CallbackProp
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import com.airbnb.epoxy.TextProp
import com.ponomarenko.themoviedb.R
import com.ponomarenko.themoviedb.base.presentation.extensions.loadImage
import com.ponomarenko.themoviedb.databinding.ViewCellRectangleImageTextBinding
import com.ponomarenko.ui.extensions.resolveResourceId
import com.redmadrobot.extensions.resources.getDimension
import com.redmadrobot.extensions.viewbinding.inflateViewBinding

@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT)
class RectangleImageTextCell @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : ConstraintLayout(context, attrs, defStyleAttr), SpacingBehavior {

    private val binding: ViewCellRectangleImageTextBinding = inflateViewBinding()

    override val handlesHorizontalSpacing = true
    override val handlesVerticalSpacing = true

    init {
        setBackgroundResource(resolveResourceId(android.R.attr.selectableItemBackground))

        val paddingHorizontal = getDimension(R.dimen.cell_padding_normal).toInt()
        val paddingVertical = getDimension(R.dimen.cell_padding_normal).toInt()
        setPadding(paddingHorizontal, paddingVertical, paddingHorizontal, paddingVertical)

        clipToPadding = false
    }

    @TextProp
    fun setImage(url: CharSequence?) {
        loadPoster(url)
    }

    private fun loadPoster(url: CharSequence?) {
        binding.cellImage.loadImage(url)
    }

    @TextProp
    fun setTitle(title: CharSequence?) {
        binding.cellTitle.text = title ?: ""
    }

    @TextProp
    fun setDescription(description: CharSequence) {
        binding.cellDescription.text = description
    }

    @ModelProp
    fun setRating(rating: Float) {
        binding.rating.rating = rating
    }

    @TextProp
    fun setRelease(release: CharSequence?) {
        binding.cellRelease.isVisible = !release.isNullOrEmpty()
        binding.cellRelease.text = release ?: ""
    }

    @CallbackProp
    fun setOnClickListener(listener: ((View) -> Unit)?) {
        super.setOnClickListener(listener)
    }
}
