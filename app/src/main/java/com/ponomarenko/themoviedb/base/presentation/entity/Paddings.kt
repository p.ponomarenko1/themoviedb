package com.ponomarenko.themoviedb.base.presentation.entity

import android.view.View
import androidx.core.view.updatePadding

data class Paddings(
    val left: Dimension? = null,
    val top: Dimension? = null,
    val right: Dimension? = null,
    val bottom: Dimension? = null,
)

fun View.updatePadding(paddings: Paddings) {
    updatePadding(
        left = paddings.left?.let(::getDimension) ?: paddingLeft,
        top = paddings.top?.let(::getDimension) ?: paddingTop,
        right = paddings.right?.let(::getDimension) ?: paddingRight,
        bottom = paddings.bottom?.let(::getDimension) ?: paddingBottom
    )
}
