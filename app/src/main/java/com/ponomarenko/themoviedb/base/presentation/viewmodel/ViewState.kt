package com.ponomarenko.themoviedb.base.presentation.viewmodel

/**
 * Маркерный интерфейс для сущностей, описывающих состояние экрана или вьюхи.
 * @see StatefulViewModel
 */
interface ViewState
