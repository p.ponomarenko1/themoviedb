package com.ponomarenko.themoviedb.base.presentation.ui

import com.redmadrobot.extensions.lifecycle.Event
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.google.android.material.snackbar.Snackbar
import com.ponomarenko.themoviedb.R
import com.ponomarenko.themoviedb.base.presentation.extensions.navigateSafe
import com.ponomarenko.themoviedb.base.presentation.entity.getString
import com.ponomarenko.themoviedb.base.presentation.viewmodel.ErrorMessageEvent
import com.ponomarenko.themoviedb.base.presentation.viewmodel.MessageEvent
import com.ponomarenko.themoviedb.base.presentation.viewmodel.NavigationEvent
import com.ponomarenko.ui.snackbar.SnackbarStyle
import com.ponomarenko.ui.snackbar.setStyle


abstract class BaseActivity(@LayoutRes layoutId: Int): AppCompatActivity(layoutId), SnackbarHandler {

    private val mainView by lazy { window.decorView }

    override fun showSnackbar(
        text: String,
        duration: Int,
        actionTitleId: Int?,
        action: ((View) -> Unit)?,
        containerResId: Int,
        anchorViewId: Int?,
        style: SnackbarStyle
    ) {
        val viewGroup = mainView.findViewById(containerResId) as? ViewGroup ?: return
        Snackbar.make(viewGroup, text, duration)
            .apply {
                actionTitleId?.let { setAction(it, action) }
                anchorView = viewGroup.findViewById(anchorViewId ?: View.NO_ID)
                setStyle(style)
            }
            .show()
    }

    /** Функция для обработки событий, которые прилетают из ViewModel. */
    @CallSuper
    protected open fun onEvent(event: Event) {
        when (event) {
            is MessageEvent -> {
                showSnackbar(
                    getString(event.message),
                    style = if (event.urgent) SnackbarStyle.UrgentMessage else SnackbarStyle.Message
                )
            }
            is ErrorMessageEvent -> showSnackbar(getString(event.message), style = SnackbarStyle.Error)
            is NavigationEvent -> handleNavigationEvent(event)
        }
    }

    protected open fun handleNavigationEvent(event: NavigationEvent) {
        val controller = findNavController(R.id.main_container)
        when (event) {
            is NavigationEvent.ToDirection -> controller.navigateSafe(event.direction, extras = event.extras)
            is NavigationEvent.PopBackStack -> controller.popBackStack(event.destinationId, event.inclusive)
            is NavigationEvent.Up -> controller.navigateUp()
        }
    }
}
