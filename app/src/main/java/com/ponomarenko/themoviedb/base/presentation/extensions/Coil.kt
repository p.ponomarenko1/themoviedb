package com.ponomarenko.themoviedb.base.presentation.extensions

import android.content.Context
import android.graphics.drawable.Drawable
import android.widget.ImageView
import coil.imageLoader
import coil.load
import coil.loadAny
import coil.request.CachePolicy
import coil.request.Disposable
import coil.request.ImageRequest
import coil.transform.CircleCropTransformation
import coil.transform.RoundedCornersTransformation
import com.ponomarenko.themoviedb.BuildConfig
import com.ponomarenko.themoviedb.R

fun ImageView.loadAvatar(data: Any?): Disposable {
    return loadAny(data, builder = ImageRequest.Builder::avatarBuilder)
}

fun ImageView.loadImage(url: CharSequence?) {
    url?.let {
        if (it.contains(".jpg", ignoreCase = true))
            this.load(BuildConfig.POSTER_BASE_URL + it) {
                imageParams(this)
            }
        else
            this.load(url.toString()) {
                imageParams(this)
            }
    }
}

private fun imageParams(builder: ImageRequest.Builder): ImageRequest.Builder {
    return with(builder) {
        crossfade(true)
        diskCachePolicy(CachePolicy.READ_ONLY)
            .placeholder(R.drawable.image_placeholder_square)
            .error(R.drawable.image_placeholder_square)
        transformations(
            RoundedCornersTransformation(
                topLeft = ROUNDED_CORNER_SIZE,
                topRight = ROUNDED_CORNER_SIZE,
                bottomLeft = ROUNDED_CORNER_SIZE,
                bottomRight = ROUNDED_CORNER_SIZE
            )
        )
    }
}

fun ImageView.loadAvatar(url: CharSequence?) {
    this.load(BuildConfig.POSTER_BASE_URL + url) {
        crossfade(true)
        diskCachePolicy(CachePolicy.READ_ONLY)
            .placeholder(R.drawable.icon_placeholder_circle)
            .fallback(R.drawable.image_avatar_44x44)
            .error(R.drawable.image_avatar_44x44)
        transformations(CircleCropTransformation())
    }
}

private fun ImageRequest.Builder.avatarBuilder() {
    crossfade(true)
    placeholder(R.drawable.icon_placeholder_circle)
    fallback(R.drawable.image_avatar_44x44)
    error(R.drawable.image_avatar_44x44)
    transformations(CircleCropTransformation())
}


fun Context.loadImage(imageUrl: String?, onResult: (Drawable?) -> Unit): Disposable {
    val request = ImageRequest.Builder(this)
        .data(imageUrl)
        .target(
            onError = { onResult(null) },
            onSuccess = { onResult(it) }
        )
        .build()
    return imageLoader.enqueue(request)
}

const val ROUNDED_CORNER_SIZE = 15F
