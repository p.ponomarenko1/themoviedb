package com.ponomarenko.themoviedb.base.presentation.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.airbnb.epoxy.CallbackProp
import com.airbnb.epoxy.ModelView
import com.airbnb.epoxy.TextProp
import com.ponomarenko.themoviedb.R
import com.ponomarenko.themoviedb.base.presentation.extensions.loadAvatar
import com.ponomarenko.themoviedb.databinding.ViewCellCircleImageTextBinding
import com.redmadrobot.extensions.resources.getDimension
import com.redmadrobot.extensions.viewbinding.inflateViewBinding

//MATCH_WIDTH_WRAP_HEIGHT
@ModelView(autoLayout = ModelView.Size.WRAP_WIDTH_WRAP_HEIGHT)
class CircleImageTextCell @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : LinearLayout(context, attrs, defStyleAttr), SpacingBehavior {

    private val binding: ViewCellCircleImageTextBinding = inflateViewBinding()

    init {
        orientation = VERTICAL
        val padding = getDimension(R.dimen.cell_padding_normal).toInt()
        setPadding(padding, padding, padding, padding)
    }

    override val handlesHorizontalSpacing = false
    override val handlesVerticalSpacing = false

    @TextProp
    fun setText(text: CharSequence) {
        binding.cellText.text = text.toString().replace(" ", "\n")
    }

    @CallbackProp
    fun setOnActionClickListener(listener: ((View) -> Unit)?) {
        binding.cellImage.setOnClickListener(listener)
    }

    @TextProp
    fun setImage(url: CharSequence?) {
        loadAvatar(url)
    }

    private fun loadAvatar(url: CharSequence?) {
        binding.cellImage.loadAvatar(url)
    }
}
