package com.ponomarenko.themoviedb.base.presentation.entity

import android.app.Activity
import android.content.Context
import android.support.annotation.StringRes
import android.view.View
import androidx.fragment.app.Fragment

sealed class Text {
    abstract fun toString(context: Context): String

    data class Plain(private val string: String) : Text() {
        override fun toString(context: Context) = string
    }

    data class Res(@StringRes private val res: Int) : Text() {
        override fun toString(context: Context) = context.getString(res)
    }
}

fun Fragment.getString(text: Text): String = text.toString(requireContext())

fun Activity.getString(text: Text): String = text.toString(this)

fun View.getString(text: Text): String = text.toString(context)
