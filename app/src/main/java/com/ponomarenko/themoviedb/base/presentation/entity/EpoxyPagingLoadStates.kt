package com.ponomarenko.themoviedb.base.presentation.entity

import androidx.paging.LoadState
import androidx.paging.LoadStates
import com.airbnb.epoxy.paging3.PagingDataEpoxyController
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

fun PagingDataEpoxyController<*>.pagingLoadStates(
    rebuildOnAppendChanged: Boolean = true,
    rebuildOnPrependChanged: Boolean = false,
    reduceStates: (oldStates: LoadStates, newStates: LoadStates) -> LoadStates = { _, newStates -> newStates },
): ReadOnlyProperty<PagingDataEpoxyController<*>, LoadStates> {
    return EpoxyPagingLoadStates(
        controller = this,
        rebuildOnAppendChanged = rebuildOnAppendChanged,
        rebuildOnPrependChanged = rebuildOnPrependChanged,
        reduceStates = reduceStates,
    )
}

@ObsoleteCoroutinesApi
private class EpoxyPagingLoadStates(
    controller: PagingDataEpoxyController<*>,
    rebuildOnAppendChanged: Boolean,
    rebuildOnPrependChanged: Boolean,
    reduceStates: (oldStates: LoadStates, newStates: LoadStates) -> LoadStates,
) : ReadOnlyProperty<PagingDataEpoxyController<*>, LoadStates> {

    private var firstStateSkipped = false
    private var loadStates = LoadStates(
        refresh = LoadState.Loading,
        prepend = LoadState.NotLoading(endOfPaginationReached = false),
        append = LoadState.NotLoading(endOfPaginationReached = false)
    )

    init {
        controller.addLoadStateListener { combinedStates ->
            if (!firstStateSkipped) {
                firstStateSkipped = true
                return@addLoadStateListener
            }
            val newStates = combinedStates.source.fix()
            val rebuildByRefresh = loadStates.refresh != newStates.refresh
            val rebuildByPrepend = rebuildOnPrependChanged && loadStates.prepend != newStates.prepend
            val rebuildByAppend = rebuildOnAppendChanged && loadStates.append != newStates.append
            loadStates = reduceStates(loadStates, newStates)
            if (rebuildByRefresh or rebuildByPrepend or rebuildByAppend) {
                controller.requestModelBuild()
            }
        }
    }

    override fun getValue(thisRef: PagingDataEpoxyController<*>, property: KProperty<*>): LoadStates = loadStates

    // Избавляемся от кейса, когда ошибка/загрузка приходят и в refresh, и в пагинации
    private fun LoadStates.fix(): LoadStates {
        return if (refresh is LoadState.NotLoading) {
            this
        } else {
            LoadStates(
                refresh = refresh,
                append = LoadState.NotLoading(true),
                prepend = LoadState.NotLoading(true)
            )
        }
    }
}
