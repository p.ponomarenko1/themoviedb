package com.ponomarenko.themoviedb.base.domain.lce

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import timber.log.Timber

@Suppress("UNCHECKED_CAST", "USELESS_CAST")
fun <T> Flow<T>.mapToLce(): Flow<Lce<T>> {
    return map { Lce.Content(it) as Lce<T> }
        .onStart { emit(Lce.Loading as Lce<T>) }
        .catch { throwable ->
            Timber.e(throwable)
            emit(Lce.Error(throwable) as Lce<T>)
        }
}

inline fun <X, Y> Flow<Lce<X>>.mapLceContent(crossinline transform: (X) -> Y): Flow<Lce<Y>> {
    return map { it.mapContent(transform) }
}
