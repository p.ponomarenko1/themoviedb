package com.ponomarenko.themoviedb.base.domain.extensions

import androidx.paging.*
import kotlinx.coroutines.flow.Flow

fun <T : Any> emulatePaging(load: suspend () -> List<T>): Flow<PagingData<T>> {
    return Pager(
        config = PagingConfig(pageSize = 1),
        pagingSourceFactory = { FakePagingSource(load) }
    ).flow
}

private class FakePagingSource<T : Any>(private val load: suspend () -> List<T>) : PagingSource<Nothing, T>() {
    @Suppress("TooGenericExceptionCaught")
    override suspend fun load(params: LoadParams<Nothing>): LoadResult<Nothing, T> {
        return try {
            LoadResult.Page(load(), null, null)
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Nothing, T>): Nothing? = null
}

// States

/** Если прошлое состояние [oldState] было [LoadState.NotLoading], оставляем его. */
fun reduceStateKeepNotLoading(oldState: LoadState, newState: LoadState): LoadState {
    return if (newState is LoadState.Loading && oldState is LoadState.NotLoading) {
        oldState
    } else {
        newState
    }
}
