package com.ponomarenko.themoviedb.base.presentation.viewmodel

import androidx.navigation.NavDirections
import androidx.navigation.Navigator
import com.ponomarenko.themoviedb.base.presentation.entity.Text
import com.redmadrobot.extensions.lifecycle.EventQueue

interface EventsDispatcher {

    val events: EventQueue

    fun showMessage(message: String, urgent: Boolean = false, dispatchToActivity: Boolean = true) {
        events.offerEvent(MessageEvent(Text.Plain(message), urgent, dispatchToActivity))
    }

    fun showMessage(message: Int, urgent: Boolean = false, dispatchToActivity: Boolean = true) {
        events.offerEvent(MessageEvent(Text.Res(message), urgent, dispatchToActivity))
    }

    fun showError(message: String, dispatchToActivity: Boolean = true) {
        events.offerEvent(ErrorMessageEvent(Text.Plain(message), dispatchToActivity))
    }

    fun showError(message: Int, dispatchToActivity: Boolean = true) {
        events.offerEvent(ErrorMessageEvent(Text.Res(message), dispatchToActivity))
    }

    fun navigateTo(direction: NavDirections, extras: Navigator.Extras? = null, rootGraph: Boolean = false) {
        events.offerEvent(NavigationEvent.ToDirection(direction, extras, rootGraph))
    }

    fun popBackStack(destinationId: Int, inclusive: Boolean = false, rootGraph: Boolean = false) {
        events.offerEvent(NavigationEvent.PopBackStack(destinationId, inclusive, rootGraph))
    }

    fun navigationUp(rootGraph: Boolean = false) {
        events.offerEvent(NavigationEvent.Up(rootGraph))
    }
}
