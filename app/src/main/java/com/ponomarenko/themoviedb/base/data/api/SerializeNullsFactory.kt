package com.ponomarenko.themoviedb.base.data.api

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonQualifier
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import java.lang.reflect.Type

class SerializeNullsFactory : JsonAdapter.Factory {
    override fun create(type: Type, annotations: Set<Annotation>, moshi: Moshi): JsonAdapter<*>? {
        val rawType = Types.getRawType(type)
        return if (rawType.isAnnotationPresent(SerializeNulls::class.java)) {
            moshi.nextAdapter<Any>(this, type, annotations).serializeNulls()
        } else {
            null
        }
    }
}

/** Enables nulls serialization for class. */
@MustBeDocumented
@JsonQualifier
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class SerializeNulls
