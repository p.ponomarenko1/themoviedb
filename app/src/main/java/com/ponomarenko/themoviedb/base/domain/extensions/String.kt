package com.ponomarenko.themoviedb.base.domain.extensions

val String.Companion.EMPTY get() = ""
