package com.ponomarenko.themoviedb.base.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.util.*


@Parcelize
data class EncodedImage(val encoded: String) : Parcelable {
    companion object {
        fun of(encoded: String?): EncodedImage? = encoded?.let(::EncodedImage)
    }

    fun decode(): ByteArray = Base64.getDecoder().decode(encoded)
}
