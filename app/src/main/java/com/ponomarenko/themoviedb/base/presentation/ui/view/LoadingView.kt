package com.ponomarenko.themoviedb.base.presentation.ui.view

import android.content.Context
import android.util.AttributeSet
import com.airbnb.epoxy.ModelCollector
import com.airbnb.epoxy.ModelView
import com.ponomarenko.themoviedb.databinding.ViewLoadingBinding
import com.redmadrobot.extensions.viewbinding.inflateViewBinding

@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_MATCH_HEIGHT)
class LoadingView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : VisibleCenterContainer(context, attrs, defStyleAttr) {

    override val centerViews = listOf(inflateViewBinding<ViewLoadingBinding>().loader)
}

fun ModelCollector.loadingView() {
    loadingView { id("loading") }
}
