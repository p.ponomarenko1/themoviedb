package com.ponomarenko.themoviedb.base.presentation.ui

import android.view.View
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.ponomarenko.themoviedb.R
import com.ponomarenko.themoviedb.base.presentation.extensions.dismissDialogByTag
import com.ponomarenko.themoviedb.base.presentation.extensions.navigateSafe
import com.ponomarenko.themoviedb.base.presentation.entity.getString
import com.ponomarenko.themoviedb.base.presentation.viewmodel.ErrorMessageEvent
import com.ponomarenko.themoviedb.base.presentation.viewmodel.MessageEvent
import com.ponomarenko.themoviedb.base.presentation.viewmodel.NavigationEvent
import com.ponomarenko.ui.snackbar.SnackbarStyle
import com.redmadrobot.extensions.lifecycle.Event

abstract class BaseFragment(@LayoutRes layoutId: Int) : Fragment(layoutId), SnackbarHandler {

    companion object {
        private const val PROGRESS_DIALOG_TAG = "progress"
    }

    private val baseActivity by lazy { activity as BaseActivity }

    protected var showModalProgress: Boolean = false
    set(value) {
        if (value == field) return
        field = value
        if (value) showModalProgress() else hideModalProgress()
    }

    override fun showSnackbar(
        text: String,
        duration: Int,
        actionTitleId: Int?,
        action: ((View) -> Unit)?,
        containerReId: Int,
        anchorViewId: Int?,
        style: SnackbarStyle
    ) {
        baseActivity.showSnackbar(
            text = text,
            duration = duration,
            actionTitleId = actionTitleId,
            action = action,
            containerResId = containerReId,
            anchorViewId = anchorViewId,
            style = style,
        )
    }

    /** Функция для обработки событий, которые прилетают из ViewModel. */
    @CallSuper
    protected open fun onEvent(event: Event) {
        when (event) {
            is MessageEvent -> {
                showSnackbar(
                    getString(event.message),
                    style = if (event.urgent) SnackbarStyle.UrgentMessage else SnackbarStyle.Message
                )
            }
            is ErrorMessageEvent -> showSnackbar(getString(event.message), style = SnackbarStyle.Error)
            is NavigationEvent -> handleNavigationEvent(event)
        }
    }

    protected open fun handleNavigationEvent(event: NavigationEvent) {
        val controller = if (event.rootGraph) getRootNavController() else getNavController()
        when (event) {
            is NavigationEvent.ToDirection -> controller.navigateSafe(event.direction, extras = event.extras)
            is NavigationEvent.PopBackStack -> controller.popBackStack(event.destinationId, event.inclusive)
            is NavigationEvent.Up -> if (!controller.navigateUp()) getRootNavController().navigateUp()
        }
    }

    protected open fun getNavController(): NavController = findNavController()

    protected open fun getRootNavController(): NavController = requireActivity().findNavController(R.id.main_container)

    private fun hideModalProgress() {
        childFragmentManager.dismissDialogByTag(PROGRESS_DIALOG_TAG)
    }

    private fun showModalProgress() {
        hideModalProgress()
        ProgressDialog.newInstance().showNow(childFragmentManager, PROGRESS_DIALOG_TAG)
    }
}
