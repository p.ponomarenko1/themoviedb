package com.ponomarenko.themoviedb.base.presentation.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import com.airbnb.epoxy.CallbackProp
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import com.airbnb.epoxy.TextProp
import com.ponomarenko.themoviedb.databinding.ViewStubBinding
import com.redmadrobot.extensions.viewbinding.inflateViewBinding

@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_MATCH_HEIGHT)
class StubView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : VisibleCenterContainer(context, attrs, defStyleAttr), SpacingBehavior {

    private val binding: ViewStubBinding = inflateViewBinding()

    override val centerViews: List<View> by lazy { listOf(binding.stubContent) }

    @TextProp
    fun setTitle(title: CharSequence?) {
        with(binding.stubTitle) {
            isVisible = title != null
            text = title
        }
    }

    @TextProp
    fun setText(text: CharSequence?) {
        with(binding.stubText) {
            isVisible = text != null
            this.text = text
        }
    }

    @ModelProp
    fun setImage(@DrawableRes imageRes: Int) {
        binding.stubImage.setImageResource(imageRes)
    }

    @CallbackProp
    fun setAction(action: Action?) {
        with(binding.stubButton) {
            isVisible = action != null
            action?.let { action ->
                setText(action.titleRes)
                setOnClickListener { action.run() }
            }
        }
    }

    data class Action(
        @StringRes val titleRes: Int,
        val run: () -> Unit,
    )
}
