package com.ponomarenko.themoviedb.base.presentation.extensions

import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager

/** Скрывает диалог по тегу если такой диалог открыт. */
fun FragmentManager.dismissDialogByTag(tag: String) {
    (findFragmentByTag(tag) as? DialogFragment)?.dismissAllowingStateLoss()
}
