package com.ponomarenko.themoviedb.base.data.api

import com.ponomarenko.themoviedb.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class AuthHeaderInterceptor @Inject constructor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
            .newBuilder()
            .url(chain.request().url.newBuilder().addQueryParameter(AUTH_HEADER, BuildConfig.API_V3_KEY).build())
            .build()
        return chain.proceed(request)
    }
}
