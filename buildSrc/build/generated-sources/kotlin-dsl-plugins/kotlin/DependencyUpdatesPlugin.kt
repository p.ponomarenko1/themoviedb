/**
 * Precompiled [dependency-updates.gradle.kts][Dependency_updates_gradle] script plugin.
 *
 * @see Dependency_updates_gradle
 */
class DependencyUpdatesPlugin : org.gradle.api.Plugin<org.gradle.api.Project> {
    override fun apply(target: org.gradle.api.Project) {
        try {
            Class
                .forName("Dependency_updates_gradle")
                .getDeclaredConstructor(org.gradle.api.Project::class.java, org.gradle.api.Project::class.java)
                .newInstance(target, target)
        } catch (e: java.lang.reflect.InvocationTargetException) {
            throw e.targetException
        }
    }
}
