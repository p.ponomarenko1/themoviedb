import com.android.build.gradle.BaseExtension

plugins {
    id("kotlin-android")
}

configure<BaseExtension> {

    setCompileSdkVersion(30)
    defaultConfig {
        minSdkVersion(28)
        targetSdkVersion(30)
        versionCode = 1
        versionName = "1.0"
        multiDexEnabled = true
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
}