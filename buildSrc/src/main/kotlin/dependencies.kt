@file:Suppress("ClassName")

object androidx {
    const val activity = "androidx.activity:activity-ktx:1.2.2"
    const val annotation = "androidx.annotation:annotation:1.2.0"
    const val appcompat = "androidx.appcompat:appcompat:1.3.0"
    const val constraintlayout = "androidx.constraintlayout:constraintlayout:2.0.4"
    const val coordinatorlayout = "androidx.coordinatorlayout:coordinatorlayout:1.1.0"
    const val core = "androidx.core:core-ktx:1.5.0"
    const val fragment = "androidx.fragment:fragment-ktx:1.3.2"
    const val material = "com.google.android.material:material:1.3.0"

    const val paging3 = "androidx.paging:paging-runtime-ktx"
    const val recyclerview = "androidx.recyclerview:recyclerview:1.1.0"
    const val swiperefreshlayout = "androidx.swiperefreshlayout:swiperefreshlayout:1.1.0"

    object hilt : Group("androidx.hilt", version = "1.0.0-beta01") {
        val navigation_fragment by this
    }

    object lifecycle : Group("androidx.lifecycle", version = "2.3.1") {
        val process by this
    }

    object navigation : Group("androidx.navigation", version = "2.3.4") {
        val fragment_ktx by this
        val ui_ktx by this
    }

    object room : Group("androidx.room", version = "2.3.0") {
        val ktx by this
        val compiler by this
    }
}

object kotlinx {
    const val coroutines_android = "org.jetbrains.kotlinx:kotlinx-coroutines-android:1.4.3"
}

object square {
    const val leakcanary = "com.squareup.leakcanary:leakcanary-android:2.7"

    object okhttp : Group("com.squareup.okhttp3", name = "okhttp", version = "4.9.1", addPrefix = false) {
        val logging_interceptor by this
    }

    object retrofit2 : Group("com.squareup.retrofit2", name = "retrofit", version = "2.9.0", addPrefix = false) {
        val converter_moshi by this
    }

    object moshi : Group("com.squareup.moshi", version = "1.12.0") {
        val kotlin_codegen by this
    }
}

object redmadrobot {
    const val core_ktx = "com.redmadrobot.extensions:core-ktx:1.3.2-0"
    const val livedata_ktx = "com.redmadrobot.extensions:lifecycle-livedata-ktx:2.3.0-0"
    const val resources_ktx = "com.redmadrobot.extensions:resources-ktx:1.2.0-1"
    const val viewbinding_ktx = "com.redmadrobot.extensions:viewbinding-ktx:4.1.2-1"
    const val fragment_args_ktx = "com.redmadrobot.extensions:fragment-args-ktx:1.3.0-0"

    const val flipper = "com.redmadrobot:flipper:2.0.1"
    const val itemsadapter = "com.redmadrobot.itemsadapter:itemsadapter-viewbinding:1.1"

    object mapmemory : Group("com.redmadrobot.mapmemory", name = "mapmemory", version = "2.0") {
        val coroutines by this
        val kapt_bug_workaround by this
    }

    object pinkman : Group("com.redmadrobot", name = "pinkman", version = "1.1.3") {
        val coroutines by this
        val ui by this
    }
}

object airbnb {
    object epoxy : Group("com.airbnb.android", "epoxy", version = "4.5.0") {
        val processor by this
        val paging3 by this
    }

    const val lottie = "com.airbnb.android:lottie:3.6.1"
}

object coil : Group("io.coil-kt", name = "coil", version = "1.1.1")

object misc {
    const val insetter = "dev.chrisbanes:insetter-ktx:0.3.1"
    const val logger = "com.orhanobut:logger:2.2.0"
    const val timber = "com.jakewharton.timber:timber:4.7.1"
    const val amplitude = "com.amplitude:android-sdk:2.30.2"
}

object google {
    object dagger : Group("com.google.dagger", version = "2.35", addPrefix = false) {
        val hilt_android by this
        val hilt_compiler by this
    }

    object firebase {
        const val bom = "com.google.firebase:firebase-bom:26.8.0"
        const val crashlytics = "com.google.firebase:firebase-crashlytics-ktx"
        const val analytics = "com.google.firebase:firebase-analytics-ktx"
        const val messaging = "com.google.firebase:firebase-messaging-ktx"
    }
}

object junit {
    object jupiter : Group("org.junit.jupiter", "junit-jupiter", version = "5.7.1") {
        val api by this
        val engine by this
        val params by this
    }
}

object mockito : Group("org.mockito", version = "3.8.0") {
    val core by this
    val inline by this
    const val kotlin = "com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0"
}