import org.gradle.api.internal.artifacts.dependencies.DefaultExternalModuleDependency
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

open class Group(
    group: String,
    name: String = group.substringAfterLast('.'),
    version: String,
    private val addPrefix: Boolean = true
) : ReadOnlyProperty<Any?, String>, DefaultExternalModuleDependency(group, name, version) {

    override fun getValue(thisRef: Any?, property: KProperty<*>): String {
        return dep(property.name.replace('_', '-'))
    }

    private fun dep(module: String): String {
        val fullName = if (addPrefix) "$name-$module" else module
        return "$group:$fullName:$version"
    }
}