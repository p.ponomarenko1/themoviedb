
plugins {
    id("dagger.hilt.android.plugin") version "2.34.1-beta" apply false
    id("androidx.navigation.safeargs.kotlin") version "2.3.4" apply false
    //id("com.google.firebase.crashlytics") version "2.6.1" apply false
    id("dependency-updates")
    id("com.google.gms.google-services") version "4.3.8" apply false
}

buildscript {
    val kotlin_version by extra("1.5.0")
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
    dependencies {
        classpath("com.android.tools.build:gradle:4.2.1")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.5.0")
        classpath("io.gitlab.arturbosch.detekt:detekt-gradle-plugin:1.17.0")
        classpath ("com.google.firebase:firebase-crashlytics-gradle:2.6.1")
        //classpath("com.google.firebase.crashlytics:2.6.1")
        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle.kts files
    }
}

tasks.register("clean",Delete::class){
    delete(rootProject.buildDir)
}
